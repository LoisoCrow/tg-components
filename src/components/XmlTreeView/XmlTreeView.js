import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';
import './XmlTreeView.sass';

const Fade = ({ in: inProp, children }) => (
  <Transition in={inProp} timeout={0}>
    {state => (
      <div
        className={`xmlNode xmlNode__collapsed--${state}`}
      >
        {children}
      </div>
    )}
  </Transition>
);

class XmlTreeView extends Component {
  toggleCollapsed = () => {
    this.props.onChange();
  };

  toggleClassEndTag = (event) => {
    if (event === 'enter') {
      this.endTag.classList.add('xmlNodeTag__active');
      this.startTag.classList.add('xmlNodeTag__active');
    } else {
      this.endTag.classList.remove('xmlNodeTag__active');
      this.startTag.classList.remove('xmlNodeTag__active');
    }
  };

  renderAtttributes = node => React.cloneElement(node, node.props);
/* <span>{`${key}="${attributes[key]}"`}</span> */
  render() {
    const { label, value, collapsed, attributes } = this.props;

    const tagAttributes = Object.keys(attributes).map(key => (
      <span key={key} className="xmlNodeTag">
        <span className="xmlNodeTag__name">{key}</span>
        =
        <span className="xmlNodeTag__value">{`"${attributes[key]}"`}</span>
      </span>
    ));

    return (
      <div className="xmlNode">
        <Fade in={collapsed}>
          <div className="xmlNode xmlNode__tag__close" onClick={this.toggleCollapsed}>
            {'<'}{label} {tagAttributes}{'>'}
            <span className="xmlNode xmlNode__tag__close--content">...</span>
            {`</${label}>`}
          </div>
        </Fade>
        <Fade in={!collapsed}>
          <div
            className="xmlNode xmlNode__tag"
            onMouseEnter={() => this.toggleClassEndTag('enter')}
            onMouseOut={() => this.toggleClassEndTag('out')}
            onClick={this.toggleCollapsed}
            ref={(node) => { this.startTag = node; }}
          >
            {'<'}{label} {tagAttributes}{'>'}
          </div>
          <div className="xmlNode xmlNode__value">
            {value}
          </div>
          <div
            className="xmlNode xmlNode__tag"
            ref={(node) => { this.endTag = node; }}
          >
            {`</${label}>`}
          </div>
        </Fade>
      </div>
    );
  }
}

XmlTreeView.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType(
    [
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.shape()),
    ],
  ).isRequired,
  collapsed: PropTypes.bool,
  attributes: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.shape(),
  ]),
};

XmlTreeView.defaultProps = {
  collapsed: false,
  attributes: [],
};

export default XmlTreeView;
