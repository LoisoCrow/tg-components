import React from 'react';
import renderer from 'react-test-renderer';
import Input from './Input';

it('renders correctly', () => {
  const tree = renderer
    .create(<Input
      label="Label"
      name="name"
    />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
