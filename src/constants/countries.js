import map from 'lodash/map';

const countries = {
  AM: 'Республика Армения',
  BY: 'Республика Беларусь',
  KZ: 'Республика Казахстан',
  KG: 'Киргизская Республика',
  RU: 'Российская Федерация',
};

export const filterOptions = map(countries, (country, code) => ({
  value: code,
  name: country,
}));

export default countries;
