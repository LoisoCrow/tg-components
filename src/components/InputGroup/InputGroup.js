import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './InputGroup.sass';

class InputGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: (props.value || props.value === 0) ? props.value : [],
    };

    this.items = [];
  }

  resetSelected = () => {
    const { onChange } = this.props;

    const newValue = '';

    this.setState({ value: newValue }, () => {
      onChange(newValue);
    });
  };

  handleChange = (newValue) => {
    const { onChange, type } = this.props;

    const selected = this.items.filter(o => o.state.checked === true);
    let newSelected = [];

    if (type === 'radio') {
      selected.forEach((object) => {
        if (object.props.value === newValue) {
          object.switchOn();
        }

        object.switchOff();
      });

      this.setState({ value: newValue }, onChange(newValue));
    } else {
      newSelected = selected;

      const values = newSelected.map(o => o.props.value);
      this.setState({ value: values }, () => onChange(values));
    }
  };

  allSelected = async () => {
    const { type } = this.props;

    if (type === 'checkbox') {
      await this.items.forEach((o) => {
        o.switchOn();
      });
    }

    this.handleChange();
  };

  renderButtons = () => {
    const { type } = this.props;

    if (type === 'checkbox') {
      return (
        <div className="input_group__buttons">
          <h4
            className="link input_group__btn_default bold"
            onClick={this.allSelected}
          >Выбрать все</h4>
          <h4
            className="steel input_group__btn_reset bold"
            onClick={this.resetSelected}
          >Сбросить</h4>
        </div>
      );
    }
    return (
      <div className="input_group__buttons">
        <h4
          className="link input_group__btn_default bold"
          onClick={this.resetSelected}
        >Сбросить</h4>
      </div>
    );
  };

  renderChildren = () => {
    this.items = [];
    const { name, children, type } = this.props;
    return children.map(Rad => React.cloneElement(
      Rad,
      {
        name,
        onChange: this.handleChange,
        ref: (c) => {
          if (c) {
            this.items.push(c);
          }
        },
        value: Rad.props.value,
        checked: type === 'radio' ? Rad.props.value === this.state.value : this.state.value.includes(Rad.props.value),
        key: Rad.props.value,
      },
    ));
  };

  render() {
    return (
      <div className="input_group">
        <div className="input_group__list">
          {this.renderChildren()}
        </div>
        {this.renderButtons()}
      </div>
    );
  }
}

InputGroup.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.array, PropTypes.string]),
  type: PropTypes.oneOf(['radio', 'checkbox']).isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};
InputGroup.defaultProps = {
  value: 0,
  onChange: () => null,
};

export default InputGroup;
