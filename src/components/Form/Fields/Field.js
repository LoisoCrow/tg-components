import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field as FinalFormField } from 'react-final-form';
import isArray from 'lodash/isArray';
import isFunction from 'lodash/isFunction';
import { composeValidators, required as requiredValidator } from '../validations';

export const Field = WrappedComponent => class extends Component {
  constructor(props) {
    super(props);
    const { required, validations } = props;
    let fieldValidations = [];

    if (required) {
      fieldValidations.push(requiredValidator);
    }

    if (isArray(validations)) {
      fieldValidations = [...fieldValidations, ...validations];
    } else if (isFunction(validations)) {
      fieldValidations.push(validations);
    }

    this.fieldValidations = fieldValidations;
  }
  render() {
    const { required, label, validations, onFocus, ...otherProps } = this.props;
    return (
      <FinalFormField
        {...otherProps}
        validate={composeValidators(...this.fieldValidations)}
        render={({ input, meta }) => {
          const { onFocus: inputOnFocus, ...otherInputProps } = input;

          return (
            <WrappedComponent
              {...otherInputProps}
              onFocus={() => {
                inputOnFocus();
                if (onFocus) {
                  onFocus();
                }
              }}
              label={label}
              error={meta.touched && meta.error ? meta.error : null}
              {...this.props}
            />
          );
        }}
      />

    );
  }
};

Field.propTypes = {
  ...Field.propTypes,
  required: PropTypes.bool,
  validations: PropTypes.oneOfType(
      [PropTypes.string, PropTypes.arrayOf(PropTypes.func), PropTypes.func],
    ),
  label: PropTypes.string.isRequired,
  onFocus: PropTypes.func,
};

Field.defaultProps = {
  required: false,
  validations: [],
  onFocus: null,
};

export default Field;
