import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Checkbox from './../../components/Checkbox/Checkbox';
import InputGroup from './../../components/InputGroup/InputGroup';
import Radio from './../../components/Radio/Radio';
import './Select.sass';

class Select extends Component {
  constructor(props) {
    super(props);

    this.state = {
      focus: false,
      value: props.value,
    };
  }

  componentWillMount() {
    document.addEventListener('click', this.handleClickOutside, false);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ value: nextProps.value });
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  getClassInput = () => {
    const { error } = this.props;
    const { focus } = this.state;
    return classNames('select__input', { select__input__error: error }, { select__input__active: focus });
  };

  getClassActiveBlock = () => {
    const { focus } = this.state;
    return classNames('select__active_block', { hide: !focus });
  };

  handleClickOutside = (e) => {
    if (!e.path.includes(this.parentComponent)) {
      this.setState({ focus: false });
    }
  };

  handleClickInput = () => {
    const { onFocus, onBlur } = this.props;
    const focus = !this.state.focus;
    this.setState({ focus });

    if (focus && onFocus) {
      onFocus();
    } else if (!focus && onBlur) {
      onBlur();
    }
  };

  handleChange = (value) => {
    const { onChange, multiSelect } = this.props;

    this.setState({
      value,
      focus: multiSelect ? this.state.focus : false,
    });

    if (onChange) {
      onChange(value);
    }
  };

  renderList = () => {
    const { options, multiSelect } = this.props;
    const InnerComponent = multiSelect ? Checkbox : Radio;

    return options.map(option => (
      <InnerComponent
        {...InnerComponent.props}
        label={option.name}
        key={option.value}
        value={option.value}
      />
    ));
  };

  renderTitle = () => {
    const { multiSelect, options, placeholder } = this.props;
    const { value } = this.state;

    let result;
    if (multiSelect && value.length) {
      result = `Выбрано ${value.length} значения`;
    } else if (!multiSelect && (value || +value === 0)) {
      const option = options.find(opt => `${opt.value}` === `${value}`);
      result = option ? option.name : placeholder;
    } else {
      result = placeholder;
    }

    return result;
  };

  render() {
    const { label, name, multiSelect, value } = this.props;

    return (
      <div className="select" ref={(c) => { this.parentComponent = c; }}>
        <div
          className={this.getClassInput()}
          onClick={this.handleClickInput}
          role="presentation"
        >
          {label && <h4>{label}</h4>}
          <div className={`select__input__content ${label ? '' : '__not-label'}`}>
            <span>{this.renderTitle()}</span>
            <i className="icon icon__arrow-down" />
          </div>
        </div>
        <div className={this.getClassActiveBlock()}>
          <div className="select__list">
            <InputGroup
              name={name}
              type={multiSelect ? 'checkbox' : 'radio'}
              onChange={this.handleChange}
              value={value}
            >
              {this.renderList()}
            </InputGroup>
          </div>
        </div>
      </div>
    );
  }
}

Select.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    name: PropTypes.string.isRequired,
  })).isRequired,
  value: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
  multiSelect: PropTypes.bool,
  error: PropTypes.string,
  placeholder: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,

};
Select.defaultProps = {
  label: '',
  value: 0,
  multiSelect: false,
  error: '',
  placeholder: 'Выберите из списка',
  onFocus: null,
  onBlur: null,
  onChange: null,
};

export default Select;
