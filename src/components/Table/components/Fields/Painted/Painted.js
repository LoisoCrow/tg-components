import PropTypes from 'prop-types';

const Painted = ({ value }) => {
  const positiveAnswer = 'да';
  const negativeAnswer = 'нет';

  const isPaintedInt = parseInt(value, 10);

  return (isPaintedInt === 1) ? positiveAnswer : negativeAnswer;
};

Painted.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
};

export default Painted;
