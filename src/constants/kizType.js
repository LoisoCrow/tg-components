export const PRODUCED_IN_COUNTRY = 1;
export const IMPORTED = 2;

export default {
  [PRODUCED_IN_COUNTRY]: 'Произведенный в стране',
  [IMPORTED]: 'Ввезенный в страну',
};
