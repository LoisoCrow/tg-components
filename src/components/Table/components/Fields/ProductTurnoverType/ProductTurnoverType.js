import PropTypes from 'prop-types';
import productTurnoverType from 'constants/productTurnoverType';

const ProductTurnoverType = ({ value }) => productTurnoverType[value] || '';

ProductTurnoverType.propTypes = {
  value: PropTypes.number.isRequired,
};

export default ProductTurnoverType;
