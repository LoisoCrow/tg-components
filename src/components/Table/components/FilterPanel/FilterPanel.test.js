import React from 'react';
import renderer from 'react-test-renderer';
import { Input } from 'components';
import FilterPanel from './FilterPanel';

jest.mock('crypto-pro', () => true);

it('renders correctly', () => {
  const tree = renderer
    .create(<FilterPanel
      onApplyPress={() => true}
    >
      <Input label="label" name="name" />
    </FilterPanel>)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
