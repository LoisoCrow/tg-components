import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Content, Title } from 'components';
import './SubHeader.sass';

class SubHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { title, children, handleBackClick, onCloseClick, subTitle } = this.props;

    return (
      <div className="SubHeader">
        <Content>
          <Title
            onBackArrowClick={handleBackClick}
            onCloseClick={onCloseClick}
            subTitle={subTitle}
          >{title}</Title>
          <div className="SubHeader__content">
            {children}
          </div>
        </Content>
      </div>
    );
  }
}

SubHeader.propTypes = {
  subTitle: PropTypes.string,
  title: PropTypes.string.isRequired,
  handleBackClick: PropTypes.func,
  onCloseClick: PropTypes.func,
};
SubHeader.defaultProps = {
  subTitle: '',
  handleBackClick: null,
  onCloseClick: null,
};

export default SubHeader;
