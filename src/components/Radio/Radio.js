import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './Radio.sass';

class Radio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: props.checked,
    };

    if (props.label) {
      this.uniqId = Math.random().toString(20).substring(10);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.checked !== this.state.checked) {
      this.setState({ checked: nextProps.checked });
    }
  }

  getClass = () => classNames('radio_button', {
    'radio_button--invalid': !this.props.valid,
    'radio_button--checked': this.state.checked,
  });

  switchOff = () => {
    this.setState({ checked: false });
  };

  switchOn = () => {
    this.setState({ checked: true });
  };

  handleChange = () => {
    const { onChange, value } = this.props;
    this.setState({ checked: true }, () => onChange(value));
  };

  render() {
    const { name, label, disabled, value, checked } = this.props;
    return (
      <div
        className={this.getClass()}
        role="presentation"
      >
        {label ?
          <label htmlFor={this.uniqId}><span>{label}</span></label>
        : null}
        <input
          type="radio"
          checked={checked}
          onChange={this.handleChange}
          name={name}
          id={this.uniqId || null}
          disabled={disabled}
          value={value}
        />
      </div>
    );
  }
}

Radio.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  name: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  checked: PropTypes.bool,
  label: PropTypes.string,
  valid: PropTypes.bool,
};

Radio.defaultProps = {
  onChange: () => true,
  name: 'defaultGroup',
  disabled: false,
  checked: false,
  label: '',
  valid: true,
};

export default Radio;
