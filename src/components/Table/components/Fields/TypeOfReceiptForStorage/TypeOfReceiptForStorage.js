import PropTypes from 'prop-types';
import typeOfReceiptForStorage from 'constants/typeOfReceiptForStorage';

const TypeOfReceiptForStorage = ({ value }) => typeOfReceiptForStorage[value] || '';

TypeOfReceiptForStorage.propTypes = {
  value: PropTypes.number.isRequired,
};

export default TypeOfReceiptForStorage;
