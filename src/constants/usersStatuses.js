import { map } from 'lodash';

const usersStatuses = {
  0: 'заблокирован',
  1: 'активен',
};

export const filterOptions = map(usersStatuses, (name, value) => ({
  value: Number(value),
  name,
}));

export default usersStatuses;
