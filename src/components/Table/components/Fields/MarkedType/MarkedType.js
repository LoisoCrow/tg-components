import PropTypes from 'prop-types';

const MarkedType = ({ value }) => {
  if (value !== null && +value === 0) {
    return 'Новый товар';
  } else if (+value === 1) {
    return 'Принят от ФЛ по договору комиссии';
  } else if (+value === 2) {
    return 'Возврат товара с поврежденным КиЗ';
  }
  return null;
};

MarkedType.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
};

export default MarkedType;
