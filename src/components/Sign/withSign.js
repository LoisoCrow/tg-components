import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// import SignModal from './SignModal';

export default (WrappedComponent, statePath, actionCreators) => class extends Component {
  constructor(props) {
    super(props);
    this.ConnectedComponent = connect(
      this.mapStateToProps,
      this.mapDispatchToProps,
    )(WrappedComponent);
  }

  mapStateToProps = state => ({
    xml: state[statePath].xml,
    base64Xml: state[statePath].base64Xml,
    certificate: state.user.certificate,
    finished: state[statePath].finished,
    // TODO: привести к обзему виду
    generateXmlAttributes: {
      loading: state[statePath].loading,
      error: state[statePath].error, // TODO: обработать
      success: state[statePath].success,
    },
    signXmlAttributes: state[statePath].signed || {},
  })

  mapDispatchToProps = dispatch => bindActionCreators(actionCreators, dispatch);

  render() {
    return (
      <this.ConnectedComponent {...this.props} />
    );
  }
};
