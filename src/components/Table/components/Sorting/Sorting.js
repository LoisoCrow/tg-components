import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './Sorting.sass';

class Sorting extends PureComponent {
  constructor(props) {
    super(props);

    this.state = this.initState(props);
  }

  componentWillReceiveProps(nextProps) {
    this.setState(this.initState(nextProps));
  }

  initState(props) {
    const {
      sort,
      sortingName,
    } = props;
    const sortObject = sort[0];
    const active = sortObject && sortObject.nameAttribute === sortingName;
    const direction = active ? sortObject.direction : 0;

    return {
      direction,
    };
  }

  handleClick = () => {
    const {
      sortHandler,
      sortingName,
    } = this.props;

    const newSort = [{
      nameAttribute: sortingName,
      direction: Number(!this.state.direction),
    }];

    sortHandler(newSort);
  };

  render() {
    const { title } = this.props;

    const sortingClasses = classnames('sorting', { '--up': this.state.direction, '--down': !this.state.direction });
    return (
      <div
        role="presentation"
        className={sortingClasses}
        onClick={() => this.handleClick()}
      >
        <span>{title}</span>
        <i className="icon icon__sort" />
      </div>
    );
  }
}

Sorting.propTypes = {
  sort: PropTypes.arrayOf(PropTypes.shape({ //eslint-disable-line
    nameAttribute: PropTypes.string,
    direction: PropTypes.number,
  })).isRequired,
  title: PropTypes.string,
  sortHandler: PropTypes.func.isRequired,
  sortingName: PropTypes.string.isRequired,
};

Sorting.defaultProps = {
  title: '',
};

export default Sorting;
