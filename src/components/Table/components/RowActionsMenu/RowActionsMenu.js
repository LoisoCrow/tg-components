import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import isFunction from 'lodash/isFunction';
import isBoolean from 'lodash/isBoolean';
import { Tooltip } from 'components';
import DropDown from './DropDown/DropDown';
import './RowActionsMenu.sass';

class RowActionsMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      focus: false,
    };
  }

  getRect = () => {
    const rect = this.button.getBoundingClientRect();
    const result = {
      left: rect.left,
      right: `${rect.right}px`,
      bottom: `${rect.bottom}px`,
      top: rect.top,
    };

    return result;
  };

  getClassIcon = () => classNames('icon_dotted', {
    icon_dotted__active: this.state.focus,
  });

  disabledFocus = () => this.setState({ focus: false });

  renderOneAction = (option) => {
    const { data } = this.props;
    const { label, icon, condition, svg, event } = option;

    const isAvailable = isFunction(condition) // eslint-disable-line
    ? condition(data)
    : (isBoolean(condition) ? condition : true);

    if (!isAvailable) return null;

    return (
      <Tooltip
        callElement={
          <div
            key={label}
            role="presentation"
            className="RowActionsMenu__oneAction RowActionsMenu__item"
            onClick={() => event(data._id, data)}
          >
            <i
              className={
                classNames(
                  { iconSvg: svg },
                  { [`iconSvg__${icon}`]: svg },
                  { [`icon__${icon}`]: !svg },
                  { icon: !svg },
                )
              }
            />
          </div>
        }
        position="left"
      >
        <span>{label}</span>
      </Tooltip>
    );
  }

  renderActionsMenu(options) {
    const { focus } = this.state;
    const { data } = this.props;

    return (
      <div className="RowActionsMenu">
        <button
          type="button"
          onClick={() => {
            this.setState({ focus: !focus });
          }}
          ref={(node) => { this.button = node; }}
        >
          <i className={this.getClassIcon()} />
        </button>
        {focus &&
          <DropDown
            disabledFocus={this.disabledFocus}
            options={options}
            rect={this.getRect()}
            data={data}
            id={data._id}
            ref={(node) => { this.dropDown = node; }}
          />
        }
      </div>
    );
  }

  render() {
    const { options } = this.props;

    if (options.length === 1) {
      return this.renderOneAction(options[0]);
    }
    return this.renderActionsMenu(options);
  }
}

RowActionsMenu.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.shape,
    label: PropTypes.string.isRequired,
    event: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    condition: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.func,
    ]),
    svg: PropTypes.bool,
  })).isRequired,
  data: PropTypes.shape().isRequired,
};

RowActionsMenu.defaultProps = {
  options: [],
};

export default RowActionsMenu;
