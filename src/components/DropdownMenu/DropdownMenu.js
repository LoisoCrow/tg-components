import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { getOffsetRect } from 'utils';
import { Dropdown } from './components';
import './DropdownMenu.sass';

class DropdownMenu extends Component {
  constructor(props) {
    super(props);

    const defaultOption = props.options.filter(o => o.default === true)[0];

    this.state = {
      focus: false,
      text: defaultOption ? defaultOption.text : props.options[0].text,
    };
  }

  onChange = (obj) => {
    this.setState({ text: obj.text }, () => {
      this.props.onChange(obj);
      this.disableFocus();
    });
  }

  getClassIcon = () => classNames('icon icon__mark_small', {
    icon__mark_small__up: this.state.focus,
  });

  getRect = () => {
    const rect = getOffsetRect(this._callElm);
    const heightCallElm = this._callElm.clientHeight;

    return { y: (rect.top + heightCallElm), x: rect.left };
  }

  toggleFocus = () => this.setState({ focus: !this.state.focus });

  disableFocus = () => this.setState({ focus: false });

  render() {
    const { options, callElement } = this.props;
    const { focus, text } = this.state;

    return (
      <div className="DropdownMenu">
        <div
          className="DropdownMenu__callElement"
          ref={(node) => { this._callElm = node; }}
          onClick={this.toggleFocus}
        >
          {callElement || <a className="link">{text}</a>}
          <i className={this.getClassIcon()} />
        </div>
        {focus && <div className="DropdownMenu__dropdown">
          <Dropdown
            options={options}
            disableFocus={this.disableFocus}
            rect={this.getRect()}
            onChange={this.onChange}
          />
        </div>}
      </div>
    );
  }
}

DropdownMenu.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    default: PropTypes.bool,
  })).isRequired,
  callElement: PropTypes.node,
  onChange: PropTypes.func.isRequired,
};
DropdownMenu.defaultProps = {
  callElement: null,
  options: [
    { default: false },
  ],
};

export default DropdownMenu;
