import map from 'lodash/map';

const typeOfReceipt = {
  1: 'Маркировка',
  2: 'Перемаркировка',
  3: 'Прием от поставщика',
  4: 'Обратный ввоз',
  5: 'Завершение описания при импорте',
  6: 'Импорт из ЕАЭС',
  7: 'Возврат от физ лица',
  8: 'Отклонение/отзыв при оптовой отгрузки',
};

export const filterOptions = map(typeOfReceipt, (name, value) => ({ value, name }));

export default typeOfReceipt;
