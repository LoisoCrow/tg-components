/**
 * Formats number from plain number or string of '1234567' format to '1 234 567' format as string.
 * @param {number||string} number
 * @returns {string} - formatted string.
 */
export const formatNumber = (number) => {
  const numberString = typeof number === 'string' ? number : number.toString();
  let formattedNumber = '';
  const length = numberString.length;
  for (let i = 0, j = length; i < length; i += 1, j -= 1) {
    if (j % 3 === 0) {
      formattedNumber += ' ';
    }
    formattedNumber += numberString[i];
  }
  return formattedNumber.trim();
};
