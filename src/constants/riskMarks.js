import { map } from 'lodash';

const riskMarks = {
  1: 'минимальный',
  2: 'средний',
  3: 'высокий',
};

export const filterOptions = map(riskMarks, (name, value) => ({
  value: Number(value),
  name,
}));

export default riskMarks;
