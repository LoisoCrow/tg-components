import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';
import isFunction from 'lodash/isFunction';
import isBoolean from 'lodash/isBoolean';
import classNames from 'classnames';
import anime from 'animejs';

class DropDown extends Component {
  componentWillMount() {
    this.root = document.createElement('div');
    this.root.classList.add('RowActionsMenu__dropdown');
    const st = this.root.style;
    const rect = this.props.rect;

    st.position = 'absolute';
    st.left = rect.left;
    st.right = rect.right;
    st.bottom = rect.bottom;
    st.width = 'max-content';
    st.height = 'max-content';

    document.body.appendChild(this.root);
    anime({
      targets: this.root,
      opacity: 1,
      duration: 2500,
    });

    document.addEventListener('click', this.handleClickOutside, false);
  }

  componentDidMount() {
    const rect = this.props.rect;
    const menuHeight = this.menu.clientHeight;
    const menuWidth = this.menu.clientWidth;
    this.root.style.top = `${rect.top - (menuHeight / 2)}px`;
    this.root.style.left = `${rect.left - menuWidth - 30}px`;
  }

  componentWillUnmount() {
    document.body.removeChild(this.root);
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  getClassItem = disabled => classNames('RowActionsMenu__item', {
    RowActionsMenu__item__disabled: disabled,
  });

  getClassIcon = (icon, svg) => classNames(
    { icon: !svg },
    { [`icon__${icon}`]: !svg },
    { iconSvg: svg },
    { [`iconSvg__${icon}`]: svg },
  );

  handleClickOutside = (e) => {
    if (!e.path.includes(this.parentComponent)) {
      this.props.disabledFocus();
    }
  }

  filterActions = (item) => {
    const { data } = this.props;
    const { condition } = item;

    const isAvailable = isFunction(condition) // eslint-disable-line
    ? condition(data)
    : (isBoolean(condition) ? condition : true);

    return isAvailable;
  }

  renderAction = (item) => {
    const { data, id } = this.props;
    const { disabled, label, icon } = item;
    const svg = !item.svg ? false : item.svg;
    return (
      <div
        className={this.getClassItem(disabled)}
        key={label}
        role="presentation"
        onClick={() => item.event(id, data)}
      >
        {item.icon && <i className={this.getClassIcon(icon, svg)} />}
        <span>{label}</span>
      </div>
    );
  }

  render() {
    const { options } = this.props;

    return (
      createPortal(
        <div ref={(node) => { this.menu = node; }}>
          {options.filter(this.filterActions).map(this.renderAction)}
        </div>, this.root)
    );
  }
}

DropDown.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.shape,
    label: PropTypes.string.isRequired,
    event: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    condition: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.func,
    ]),
    svg: PropTypes.bool,
  })).isRequired,
  rect: PropTypes.shape({
    left: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    right: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    top: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    bottom: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }).isRequired,
  data: PropTypes.shape().isRequired,
  id: PropTypes.string.isRequired,
};

export default DropDown;
