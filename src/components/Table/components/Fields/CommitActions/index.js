import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import classNames from 'classnames';
import { Tooltip } from 'components';
import './CommitActions.sass';

const statuses = {
  commits: {
    icon: 'take',
    tooltip: 'Принять КиЗ',
  },
  awaits: {
    icon: 'hourglass',
    tooltip: 'Оставить КиЗ в списке приёмки',
  },
  returned: {
    icon: 'reject',
    tooltip: 'Отклонить КиЗ (вернуть эмитенту)',
  },
};

class CommitActions extends Component {
  getIcoClassName = (status, value) => classNames(
    'iconSvg',
    `iconSvg__${statuses[status].icon}`,
    { '--selected': status === value },
  )

  renderButton = (status) => {
    const { row: { number_kiz }, value, setStatus } = this.props;
    return (
      <div
        onClick={() => setStatus(status, number_kiz)}
        className="CommitActions__item"
      >
        <Tooltip
          callElement={<i
            className={this.getIcoClassName(status, value)}
          />}
          position="top"
        >
          <div>{statuses[status].tooltip}</div>
        </Tooltip>
      </div>
    );
  }

  render() {
    return <div className="CommitActions">{Object.keys(statuses).map(this.renderButton)}</div>;
  }
}

CommitActions.propTypes = {
  row: PropTypes.shape({
    number_kiz: PropTypes.string,
  }).isRequired,
  setStatus: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default CommitActions;

