import map from 'lodash/map';

const individCategories = {
  1: 'С индивидуализацией',
  0: 'Без индивидуализации',
};

export const filterOptions = map(individCategories, (name, value) => ({
  value: parseInt(value, 10),
  name,
}));

export default individCategories;
