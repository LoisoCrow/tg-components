// import React, { Component } from 'react';
import PropTypes from 'prop-types';

import kizCategories from 'constants/kizCategories';

const findKiz = (value) => {
  const category = kizCategories.find(kiz => kiz.kind_kiz === value);
  if (category) {
    return category.label;
  }
  return '';
};

const KizType = ({ value }) => {
  if (typeof value === 'string') {
    if (value === '') {
      return '';
    }

    const valueInt = parseInt(value, 10);
    if (!isNaN(valueInt) && valueInt <= kizCategories.length) {
      return findKiz(valueInt);
    }
    return '';
  }
  return findKiz(value);
};

KizType.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
};

export default KizType;
