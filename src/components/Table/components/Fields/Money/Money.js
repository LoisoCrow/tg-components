import React from 'react';
import { moneyValueToRub } from 'utils';

const Money = ({ value }) => <span>{moneyValueToRub(value)} руб.</span>;

export default Money;
