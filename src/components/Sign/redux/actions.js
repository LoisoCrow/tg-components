import { base64ToUtf8 } from 'src/utils';
import * as CryptoPro from 'crypto-pro';
import config from 'config';
import { fetch, send } from '../service';

const request = type => ({
  type,
});
const receive = (type, { xml, base64Xml }) => ({
  type,
  payload: {
    xml,
    base64Xml,
  },
});
const error = (type, errorMessage) => ({
  type,
  payload: errorMessage,
});
const signRequest = type => ({
  type,
});
const signSuccess = type => ({
  type,
});
const signError = (type, errorMessage) => ({
  type,
  payload: errorMessage,
});

export default (types, actionCode, { fetchEndpoint, sendEndpoint }, api) => {
  const generateXml = data => async (dispatch) => {
    dispatch(request(types.request));
    try {
      const response = await fetch(data, fetchEndpoint, actionCode, api);
      if (response.success) {
        const base64Xml = response.data.base64Xml;
        const xml = base64ToUtf8(base64Xml);
        dispatch(receive(types.receive, { xml, base64Xml }));
      } else {
        dispatch(error(types.error, response.error));
      }
    } catch (e) {
      dispatch(error(types.error, e));
    }
  };
  const signXml = (base64Xml, certificate) => async (dispatch) => {
    dispatch(signRequest(types.signRequest));
    try {
      const requestData = {
        xml: base64Xml,
      };
      requestData.signature = config.cryptographyEnabled
          ? await CryptoPro.call('signData', certificate.origin.thumbprint, base64Xml)
          : 'fake';

      const response = await send(requestData, sendEndpoint, actionCode, api);

      if (response.success) {
        dispatch(signSuccess(types.confirm));
      } else {
        dispatch(signError(types.signError, response.error));
      }
    } catch (e) {
      dispatch(signError(types.signError, e));
    }
  };
  return {
    generateXml,
    signXml,
  };
};
