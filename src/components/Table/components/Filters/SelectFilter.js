import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Select } from 'components';

class SelectFilter extends Component {
  onChange = (value) => {
    this.props.onChange(value);
  }
  render() {
    return (
      <Select {...this.props} onChange={this.onChange} />
    );
  }
}

SelectFilter.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
};

SelectFilter.defaultProps = {
  filter: true,
};

export default SelectFilter;
