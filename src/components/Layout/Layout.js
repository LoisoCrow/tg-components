import React from 'react';
import './Layout.sass';

const Layout = ({ children }) => (
  <div className="layout">{children}</div>
);

export default Layout;
