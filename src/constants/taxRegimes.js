const taxRegimes = [
  {
    number: 1,
    name: 'Общая система налогообложения',
    abbreviation: 'ОСН',
  },
  {
    number: 2,
    name: 'Упрощенная система налогообложения',
    abbreviation: 'УСН',
  },
  {
    number: 3,
    name: 'Единый налог на вмененный доход',
    abbreviation: 'ЕНВД',
  },
  {
    number: 4,
    name: 'Патентная система налогообложения',
    abbreviation: 'ПСН',
  },
];

export const filterOptions = taxRegimes.map(regime => ({
  value: regime.number,
  name: regime.name,
}));

export default taxRegimes;
