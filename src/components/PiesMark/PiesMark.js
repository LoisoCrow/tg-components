import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Chart, Transform, Pies, Animate } from 'rumble-charts';
import './PiesMark.sass';

class PiesMark extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.data = [{ data: props.data }];
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps.data)) {
      this.data = [{ data: nextProps.data }];
    }
  }

  render() {
    const { data, width, height, combined, children } = this.props;

    if (isEmpty(data)) {
      return (
        <div className="PiesMark__no-data">
          <h2 className="text-center">Нет данных</h2>
        </div>
      );
    }

    return (
      <div className="PiesMark">
        <Chart width={width} height={height} series={this.data} style={{ paddingBottom: `${20}px` }}>
          <filter id="_PiesMarkFilter" width="200%" height="200%">
            <feOffset result="offOut" in="SourceGraphic" dx="3" dy="3" />
            <feGaussianBlur result="blurOut" in="offOut" stdDeviation="5" />
            <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
          </filter>
          <Animate ease="bounce" _ease="circle-out-in">
            <Transform method={['transpose', 'stackNormalized']}>
              <Pies
                colors="category10"
                combined={combined}
                innerRadius="90%"
                padAngle={0.035}
                cornerRadius={10}
                innerPadding={2}
                pieVisible
                pieStyle={{ filter: 'url(#_PiesMarkFilter)' }}
              />
            </Transform>
          </Animate>
        </Chart>
        <div className="PiesMark__children">
          {children}
        </div>
      </div>
    );
  }
}

PiesMark.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    color: PropTypes.string,
    y: PropTypes.number,
  })),
  width: PropTypes.number,
  height: PropTypes.number,
  combined: PropTypes.bool,
};
PiesMark.defaultProps = {
  data: [],
  width: 220,
  height: 200,
  combined: true,
};

export default PiesMark;
