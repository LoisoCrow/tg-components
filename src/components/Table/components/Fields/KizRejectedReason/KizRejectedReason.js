import { allRejectionReasons } from 'constants/rejectionReasons';
import PropTypes from 'prop-types';

const KizRejectedReason = ({ value }) => allRejectionReasons[value] || '';

KizRejectedReason.propTypes = {
  value: PropTypes.number.isRequired,
};

export default KizRejectedReason;
