const config = {
  baseUrl: 'https://mk2b.studio-tg.ru', // 'http://lk3-test.ru:9501', // https://mk2b.studio-tg.ru
  supportEmail: 'support@email.com',
  authToken: 'authToken',
  authName: 'authorized',
  authBearer: 'bearer',
  defaultFormat: 'DD.MM.YYYY',
  serverFormat: 'YYYY-MM-DD',
  cryptographyEnabled: true,
};

export default Object.freeze(config);
