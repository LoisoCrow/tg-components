import React from 'react';
import renderer from 'react-test-renderer';
import Table from './Table';

jest.mock('crypto-pro', () => true);

it('renders correctly', () => {
  const MockElement = ({ value, sign }) => (<div>{sign}{value}{sign}</div>);

  const columns = [
    {
      header: 'Name',
      field: 'name',
      width: 200,
      component: MockElement,
      componentProps: {
        sign: '+',
      },
      protected: true,
    },
  ];

  const data = [
    { name: 'John', _id: '1' },
    { name: 'Alice', _id: '2' },
    { name: 'Bob', _id: '3' },
  ];


  const tree = renderer
    .create(<Table
      columns={columns}
      data={data}
    />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
