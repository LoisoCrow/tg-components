import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ToolTip from 'react-portal-tooltip';
import omit from 'lodash/omit';
import './Tooltip.sass';

class Tooltip extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isTooltipActive: false,
    };

    this.id = `id${Math.random().toString(20).substring(10)}`;
  }

  componentWillMount = () => {
    if (this.props.active) {
      this.setState({ isTooltipActive: true });
    }
  };

  toggleState = (isTooltipActive) => {
    if (!this.props.active) {
      this.setState({ isTooltipActive });
    }
  };

  render() {
    const { callElement, children, header } = this.props;
    const { isTooltipActive } = this.state;

    const props = omit(this.props, ['callElement', 'children', 'active', 'useHover']);


    return (
      <div className="tooltip">
        <div
          className="tooltip__call_element"
          onMouseEnter={() => this.toggleState(true)}
          onMouseLeave={() => this.toggleState(false)}
          id={this.id}
        >
          {callElement}
        </div>

        <ToolTip {...props} active={isTooltipActive} parent={`#${this.id}`}>
          {header && <div className="tooltip__header"><h2>{header}</h2></div> }
          {children}
        </ToolTip>
      </div>
    );
  }
}

Tooltip.propTypes = {
  callElement: PropTypes.element,
  group: PropTypes.string,
  children: PropTypes.element,
  active: PropTypes.bool,
  position: PropTypes.oneOf(['top', 'right', 'bottom', 'left']),
  arrow: PropTypes.oneOf(['center', 'right', 'left', 'top', 'bottom']),
  tooltipTimeout: PropTypes.number,
  style: PropTypes.object,
  useHover: PropTypes.func,
  header: PropTypes.string,
};
Tooltip.defaultProps = {
  callElement: <span>Показать</span>,
  group: 'default',
  children: <span>This is the content of the tooltip</span>,
  active: false,
  position: 'right',
  arrow: 'center',
  tooltipTimeout: 500,
  header: '',
  useHover: () => true,
  style: {
    style: {
      background: '#fff',
      padding: `${5}px ${10}px`,
      filter: 'drop-shadow(0 12px 15px rgba(30, 30, 30, .4))',
      boxShadow: 'none',
      border: 'none',
    },
    arrowStyle: {
      boxShadow: 'none',
      filter: 'drop-shadow(0 12px 15px rgba(30, 30, 30, .4))',
      color: '#fff',
      borderColor: false,
    },
  },
};

export default Tooltip;
