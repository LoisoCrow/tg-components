import PropTypes from 'prop-types';

const Marked = ({ value }) => {
  const positiveAnswer = 'да';
  const negativeAnswer = 'нет';

  const isMarkedInt = parseInt(value, 10);

  return (isMarkedInt === 1) ? positiveAnswer : negativeAnswer;
};

Marked.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
};

export default Marked;
