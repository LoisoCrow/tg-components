import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { countableEnding } from 'utils';
import { Footer } from 'components';
import PaginationComponent from '../Pagination/Pagination';
import ItemsOnPage from '../ItemsOnPage/ItemsOnPage';
import MoreActionsButton from '../MoreActionsButton/MoreActionsButton';
import './PaginationPanel.sass';


class PaginationPanel extends Component {
  getContent(pagination) {
    return (
      <div className="control_panel_table">
        {pagination && this.renderPaginationAndItems()}
        {this.getSummaryLine()}
      </div>
    );
  }

  getSummaryLine = () => {
    const { selectedLength: count } = this.props;

    if (count === 0) {
      return null;
    }

    const selected = (count % 10 === 1) && (count % 100 !== 11)
      ? 'Выбран'
      : 'Выбрано';

    const ending = countableEnding(
      count,
      ['документ', 'документа', 'документов'],
    );

    return (
      <span className="selectedDocsCount">
        {`${selected} ${count} ${ending}`}
      </span>
    );
  };

  renderPaginationAndItems = () => {
    const {
      count,
      filteredCount,
      items,
      paginationOffset,
      paginationLimit,
      setPagination: onPageChange,
    } = this.props;

    return (count === 0 || filteredCount === 0) ? null : (
      <div className="control_panel_table__wrapper">
        <PaginationComponent
          count={filteredCount >= 0 ? filteredCount : count}
          paginationLimit={paginationLimit}
          paginationOffset={paginationOffset}
          onPageChange={onPageChange}
        />
        <ItemsOnPage
          items={items}
          value={paginationLimit}
          onChange={onPageChange}
        />
      </div>
    );
  }

  render() {
    const { actions: { first, second, others }, pagination } = this.props;
    return (
      <Footer>
        <div className="tablePaginationWrapper">
          {this.getContent(pagination)}
          <div className="actions">
            {others ? <MoreActionsButton
              buttons={others}
            /> : null}
            {second}
            {first}
          </div>
        </div>
      </Footer>
    );
  }
}

PaginationPanel.propTypes = {
  count: PropTypes.number.isRequired,
  selectedLength: PropTypes.number.isRequired,
  paginationLimit: PropTypes.number.isRequired,
  paginationOffset: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.number),
  setPagination: PropTypes.func,
  filteredCount: PropTypes.number.isRequired,
  actions: PropTypes.shape({
    first: PropTypes.element,
    second: PropTypes.element,
    others: PropTypes.arrayOf(PropTypes.element),
  }),
  pagination: PropTypes.bool,
};

PaginationPanel.defaultProps = {
  items: [1, 2, 10, 50, 100, 1000],
  actions: {},
  pagination: true,
  setPagination: () => {},
};

export default PaginationPanel;
