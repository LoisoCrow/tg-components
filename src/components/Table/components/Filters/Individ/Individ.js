import React, { Component } from 'react';
import { filterOptions as individCategories } from 'constants/individOptions';
import SelectFilter from '../SelectFilter';

class Individ extends Component {
  onChange = (value) => {
    const { onChange } = this.props;

    const valueInt = parseInt(value, 10);
    onChange(valueInt);
  }

  render() {
    return (
      <SelectFilter
        {...this.props}
        onChange={this.onChange}
        options={individCategories}
        label=""
        placeholder="Все"
      />
    );
  }
}

Individ.defaultProps = {
  value: 2,
  name: 'individ',
};

export default Individ;
