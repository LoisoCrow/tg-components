import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './MoreActionsButton.sass';

class MoreActionsButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
    };
  }

  componentWillMount() {
    document.addEventListener('click', this.handleClickOutside, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  handleClickOutside = (e) => {
    const selectedBlock = this.block;

    if (!e.path.includes(selectedBlock)) {
      this.setState({ isFocused: false });
    }
  };

  render() {
    const { buttons } = this.props;
    const { isFocused } = this.state;

    return (
      <div
        className="component-more-actions-button"
        ref={(c) => {
          this.block = c;
        }}
      >
        <div
          role="presentation"
          className="presentation"
          onClick={() => this.setState({ isFocused: !isFocused })}
        >
          <i className={`icon icon__arrow-up ${!isFocused ? '' : 'active'}`} />
          <span className="label">
            {!isFocused ? 'Еще действия' : 'Свернуть'}
          </span>
        </div>
        <div className={`focus ${!isFocused ? 'inVisible' : ''}`}>
          <div className="buttons">
            {React.Children.map(buttons, (button, index) => React.cloneElement(button, { key: `button_${index}` }))}
          </div>
          <i className="icon icon__mark_lighter" />
        </div>
      </div>
    );
  }
}

MoreActionsButton.propTypes = {
  buttons: PropTypes.arrayOf(PropTypes.element),
};
MoreActionsButton.defaultProps = {
  buttons: [],
};

export default MoreActionsButton;
