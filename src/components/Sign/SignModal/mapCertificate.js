import { toDefaultFormat } from 'utils';
import pickBy from 'lodash/pickBy';
import isNil from 'lodash/isNil';

export default (certificate) => {
  if (!certificate) return {};
  return pickBy({
    'Кому выдан': certificate['Владелец'],
    ИНН: certificate['ИНН'],
    КПП: certificate['КПП'],
    'Выписан на': certificate['Выписан на'],
    'Кем выдан': certificate['Выдан'],
    'Срок действия': toDefaultFormat(certificate.validTo),
  }, value => !isNil(value));
};
