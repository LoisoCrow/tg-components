import PropTypes from 'prop-types';
import countries from 'constants/countries';

const Country = ({ value }) => countries[value] || '';

Country.propTypes = {
  value: PropTypes.string.isRequired,
};

export default Country;
