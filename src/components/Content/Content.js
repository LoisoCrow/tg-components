import React from 'react';
import classnames from 'classnames';
import './Content.sass';

const Content = ({ children, className, ...otherProps }) => (
  <div className={classnames('content', { [className]: className })} {...otherProps}>{children}</div>
);

export default Content;
