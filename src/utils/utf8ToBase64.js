import utf8 from 'utf8';
import base64 from 'base-64';

export default (text) => {
  const bytes = utf8.encode(text);
  return base64.encode(bytes);
};
