import PropTypes from 'prop-types';
import { tnvedCategories } from '../../../../../constants';

const Tnved = ({ value }) => (value ? `${value} - ${tnvedCategories[value]}` : '');

Tnved.propTypes = {
  value: PropTypes.string.isRequired,
};

export default Tnved;
