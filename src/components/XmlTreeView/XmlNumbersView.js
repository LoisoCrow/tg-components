import React, { Component } from 'react';
import { Transition } from 'react-transition-group';

const Fade = ({ in: inProp, children }) => (
  <Transition in={inProp} timeout={0}>
    {state => (
      <div
        className={`xmlNode__collapsed--${state}`}
      >
        {children}
      </div>
    )}
  </Transition>
);

class XmlNumbersView extends Component {
  toggleCollapsed = () => {
    this.props.onChange();
  };

  toggleClassEndTag = () => {
    this.endTag.classList.toggle('xmlNodeNumbers__hovered');
    this.startTag.classList.toggle('xmlNodeNumbers__hovered');
  };

  render() {
    const { startTagNumber, endTagNumber, collapsed, value } = this.props;

    return (
      <div>
        <Fade in={collapsed}>
          <div onClick={this.toggleCollapsed} className="xmlNodeNumbers__expand">
            {`${startTagNumber}`}
          </div>
        </Fade>
        <Fade in={!collapsed}>
          <div
            onMouseEnter={() => this.toggleClassEndTag()}
            onMouseOut={() => this.toggleClassEndTag()}
            onClick={this.toggleCollapsed}
            ref={(node) => { this.startTag = node; }}
            className="xmlNodeNumbers__turn"
          >
            {`${startTagNumber}`}
          </div>
          <div>
            {typeof value === 'string' ? startTagNumber + 1 : value}
          </div>
          <div
            ref={(node) => { this.endTag = node; }}
          >
            {`${endTagNumber}`}
          </div>
        </Fade>
      </div>
    );
  }
}

export default XmlNumbersView;
