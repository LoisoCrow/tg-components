import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'components';
import './DropdownButton.sass';

class DropdownButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
    };
  }

  componentWillMount() {
    document.addEventListener('click', this.handleClickOutside, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  handleClickOutside = (e) => {
    const selectedBlock = this.block;

    if (!e.path.includes(selectedBlock)) {
      this.setState({ isFocused: false });
    }
  };

  render() {
    const {
      bgColor,
      type,
      children,
      icon,
      disabled,
      focusChildren,
    } = this.props;
    const { isFocused } = this.state;

    return (
      <div
        className="component-drop-down-button"
        ref={(c) => {
          this.block = c;
        }}
      >
        <Button
          bgColor={bgColor}
          type={type}
          icon={icon}
          disabled={disabled}
          onPress={() => this.setState({ isFocused: !isFocused })}
        >
          {children}
        </Button>
        <div className={`focus ${!isFocused ? 'inVisible' : ''}`}>
          <div>{focusChildren}</div>
          <i className="icon icon__mark_lighter" />
        </div>
      </div>
    );
  }
}

DropdownButton.propTypes = {
  bgColor: PropTypes.oneOf(['blue', 'orange', 'gray']),
  type: PropTypes.oneOf(['submit', 'button', 'reset']),
  children: PropTypes.string,
  icon: PropTypes.string,
  disabled: PropTypes.bool,
  focusChildren: PropTypes.arrayOf(PropTypes.element),
};
DropdownButton.defaultProps = {
  ...Button.defaultProps,
  focusChildren: [],
};

export default DropdownButton;
