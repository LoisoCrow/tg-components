import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Title.sass';

export default class Title extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.func,
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.element),
    ]).isRequired,
    subTitle: PropTypes.string,
    onBackArrowClick: PropTypes.func,
    onCloseClick: PropTypes.func,
    link: PropTypes.shape({
      title: PropTypes.string,
      link: PropTypes.any,
    }),
  }

  static defaultProps = {
    subTitle: '',
    onBackArrowClick: null,
    onCloseClick: null,
    link: null,
  }

  renderBackArrow = () => {
    const { onBackArrowClick } = this.props;
    if (!onBackArrowClick) {
      return null;
    }
    return (
      <div className="page__title__back">
        <button onClick={() => { onBackArrowClick(); }}>
          <i className="icon icon__blue icon__back_page" />
        </button>
      </div>
    );
  }

  renderCloseButton = () => {
    const { onCloseClick } = this.props;
    if (!onCloseClick) {
      return null;
    }
    return (
      <i
        onClick={() => onCloseClick()}
        className="icon icon__close icon__orange"
      />
    );
  }

  renderSubtitle = () => {
    const { subTitle } = this.props;
    if (!subTitle) {
      return null;
    }
    return <h2 className="Title__sub-title">{subTitle}</h2>;
  }

  renderLink = () => {
    const { link } = this.props;
    if (!link) {
      return null;
    }
    return <Link to={link.link} className="link page__title__text__link">{link.title}</Link>;
  }

  render() {
    const { children } = this.props;
    return (
      <div className="page__title">
        <div className="page__title__text">
          <h1>{children}</h1>
          {this.renderSubtitle()}
          {this.renderLink()}
        </div>
        {this.renderCloseButton()}
        {this.renderBackArrow()}
      </div>
    );
  }
}
