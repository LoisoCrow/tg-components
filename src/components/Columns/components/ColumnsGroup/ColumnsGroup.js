import React from 'react';
import { PropTypes } from 'prop-types';
import './ColumnsGroup.sass';

const ColumnsGroup = ({ children, className, title, width, isColumnsGroup, ...otherProps }) => (
  <div className={`columns__group ${className}`} style={{ width }} {...otherProps}>
    {title && <span className="columns__group__title">{title}</span>}
    <div className="columns__group__body">
      {children}
    </div>
  </div>
);

ColumnsGroup.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element,
  ]),
  title: PropTypes.string,
  width: PropTypes.string,
  isColumnsGroup: PropTypes.bool,
};

ColumnsGroup.defaultProps = {
  indicator: 'ColumnsGroup',
  className: '',
  children: null,
  width: `${100}%`,
  title: '',
  isColumnsGroup: true,
};

export default ColumnsGroup;
