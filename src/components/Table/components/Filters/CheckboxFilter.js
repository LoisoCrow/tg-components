import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Checkbox } from 'components';

class CheckboxFilter extends Component {
  onChange = (checked) => {
    const value = checked ? 1 : 0;

    this.props.onChange(value);
  }

  render() {
    const { value } = this.props;

    return (
      <Checkbox
        {...this.props}
        onChange={this.onChange}
        checked={value === 1}
      />
    );
  }
}

CheckboxFilter.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

CheckboxFilter.defaultProps = {
  filter: true,
};

export default CheckboxFilter;

