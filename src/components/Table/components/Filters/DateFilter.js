import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { RangeDatePicker } from 'components';

class DateFilter extends Component {
  onChange = (date) => {
    const { startDate, endDate } = date.value;

    this.props.onChange({
      from: startDate,
      to: endDate,
    });
  };

  render() {
    let start;
    let end;

    const date = this.props.value;

    try {
      start = date.from;
    } catch (exception) {
      start = null;
    }

    try {
      end = date.to;
    } catch (exception) {
      end = null;
    }

    return (
      <RangeDatePicker
        {...this.props}
        onChange={this.onChange}
        startDate={start || null}
        endDate={end || null}
      />
    );
  }
}

DateFilter.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

DateFilter.defaultProps = {
  filter: true,
};

export default DateFilter;
