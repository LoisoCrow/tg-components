import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './CollapsedContainer.sass';

class CollapsedContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: props.collapsed,
      textCollapsed: props.collapsed ? props.openText : props.closeText,
    };
  }

  componentWillUpdate = (nextProps, nextState) => {
    const { collapsed } = this.state;
    const { closeAction, openAction } = this.props;

    if (!collapsed && nextState.collapsed) {
      openAction();
    } else if (collapsed && !nextState.collapsed) {
      closeAction();
    }
  }

  getClassCollapsedBlock = () => classNames('CollapsedContainerMain', {
    CollapsedContainerMain__opened: !this.state.collapsed,
    CollapsedContainerMain__closed: this.state.collapsed,
  });

  getTextButton = () => {
    const { collapsed } = this.state;
    const { openText, closeText } = this.props;

    if (collapsed) {
      return openText;
    }
    return closeText;
  }

  toggleCollapsed = () => this.setState({ collapsed: !this.state.collapsed });

  render() {
    const { children } = this.props;

    return (
      <div className="CollapsedContainer">
        <div className="CollapsedContainer__button">
          <a
            onClick={this.toggleCollapsed}
            ref={(node) => { this.button = node; }}
          >{this.getTextButton()}</a>
        </div>
        <div className={this.getClassCollapsedBlock()}>
          {children}
        </div>
      </div>
    );
  }
}

CollapsedContainer.propTypes = {
  children: PropTypes.any.isRequired,
  openText: PropTypes.string,
  closeText: PropTypes.string,
  closeAction: PropTypes.func,
  openAction: PropTypes.func,
  collapsed: PropTypes.bool,
};
CollapsedContainer.defaultProps = {
  openText: 'Показать полную информацию',
  closeText: 'Скрыть информацию',
  closeAction: () => true,
  openAction: () => true,
  collapsed: true,
};

export default CollapsedContainer;
