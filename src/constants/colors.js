export const RED = 1;
export const GREEN = 2;

export const colors = {
  [RED]: 'red',
  [GREEN]: 'green',
};
