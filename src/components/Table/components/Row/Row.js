import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import classnames from 'classnames';
import { Cell, RowActionsMenu } from '../../components';
import { Checkbox } from '../../../../components';
import { documentTypes } from '../../../../constants';

class Row extends Component {
  getFieldData = (field) => {
    const { data } = this.props;
    const splitField = field.split('.');
    return this.findData(data, splitField);
  }

  findData = (data, fieldArray) => {
    if (!data || !fieldArray) {
      return '';
    }
    let newData = { ...data };
    for (let i = 0; i < fieldArray.length; i += 1) {
      if (!newData[fieldArray[i]]) {
        return '';
      }
      newData = newData[fieldArray[i]];
    }
    return typeof newData === 'object' ? '' : newData;
  }

  formRow = () => {
    const {
      columns,
      data,
      disabled,
    } = this.props;

    return columns.filter(f => !f.hidden).map((cell, index, filteredColumns) => {
      const isLastColumn = filteredColumns.length - 1 === index;
      let flexible = cell.hidden ? false : cell.flexible;
      if (flexible) {
        this.hasFlexibleColumn = true;
      }
      if (isLastColumn && !this.hasFlexibleColumn) {
        flexible = true;
      }
      return (
        <Cell
          header={cell.header}
          editable={cell.editable}
          disabled={disabled}
          key={cell.field}
          value={this.getFieldData(cell.field)}
          component={cell.component}
          componentProps={cell.componentProps}
          row={cell.dependent ? data : null}
          width={cell.width}
          flexible={flexible}
          className="table__info-td"
        />
      );
    });
  };

  formToggleCell = () => {
    const {
      disabled,
      rowActions,
      data,
    } = this.props;

    const className = classnames('table__editable-td', {
      'td-disabled': disabled,
    });

    return (
      <td className={className}>
        <div className="td-inner">
          {(rowActions && rowActions.length && !disabled)
            ? <RowActionsMenu
              options={rowActions}
              data={data}
            />
            : null}
        </div>
      </td>
    );
  };

  select = (add) => {
    const {
      data,
      multiselect,
      onSelect,
      selected,
    } = this.props;

    if (!multiselect) {
      onSelect([data]);
      return;
    }

    if (add) {
      onSelect([...selected, data]);
    } else {
      const index = selected.findIndex(row => row._id === data._id);
      onSelect([
        ...selected.slice(0, index),
        ...selected.slice(index + 1),
      ]);
    }
  };

  formCheckboxCell = () => {
    const {
      checkboxName,
      data,
      selected,
      multiselect,
      disabled,
    } = this.props;

    const checked =
      !!selected.find(item => item._id === data._id);

    const tooltip = disabled && data.last_doc.action_id
    ? `КиЗ участвует в отправленной, но не подтвержденной заявке по операции
    "${documentTypes.find(type => type.action_id === data.last_doc.action_id).name}". 
    Действия с КиЗ запрещены до рассмотрения заявки.`
    : null;

    return (
      <Cell
        disabled={disabled}
        key={data._id}
        value={''}
        data={data}
        component={Checkbox}
        componentProps={{
          name: checkboxName,
          onChange: this.select,
          data,
          checked,
          multiselect,
          selected,
        }}
        className="table__checkbox-td"
        tooltip={tooltip}
      />
    );
  };

  formEmptyCell = () => <Cell key="empty-cell" className="table__info-td table__info-td-empty" />

  render() {
    const {
      toggleColumns,
      selectColumn,
      disabled,
    } = this.props;
    this.hasFlexibleColumn = false;
    const rowClassName = classnames({ row__disabled: disabled });
    return (
      <tr className={rowClassName}>
        {selectColumn ? this.formCheckboxCell() : this.formEmptyCell()}
        {this.formRow()}
        {toggleColumns ? this.formToggleCell() : null}
      </tr>
    );
  }
}

Row.propTypes = {
  data: PropTypes.object.isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({
    header: PropTypes.string.isRequired,
    field: PropTypes.string.isRequired,
    hidden: PropTypes.bool,
    sorting: PropTypes.bool,
    width: PropTypes.number.isRequired,
    component: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.func,
    ]),
    componentProps: PropTypes.object,
    flexible: PropTypes.bool,
  })).isRequired,
  toggleColumns: PropTypes.bool.isRequired,
  selectColumn: PropTypes.bool.isRequired,
  checkboxName: PropTypes.string,
  onSelect: PropTypes.func,
  multiselect: PropTypes.bool,
  selected: PropTypes.array,
  disabled: PropTypes.bool,
  rowActions: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.string,
      label: PropTypes.string,
      event: PropTypes.func,
    }),
  ),
};

Row.defaultProps = {
  toggleActions: [],
  disabled: false,
  checkboxName: null,
  onSelect: () => true,
  multiselect: true,
  selected: [],
  onRowDelete: () => true,
  rowCanBeDeleted: false,
  rowActions: [],
};

export default Row;
