import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Loader.sass';

class Loader extends Component {

  renderContainer() {
    return (
      <div className="loading__container">
        {this.renderLoader()}
      </div>
    );
  }

  renderLoader() {
    return (
      <div className="loading" />
    );
  }

  render() {
    const { withContainer, visible } = this.props;

    if (!visible) return null;

    return withContainer ? this.renderContainer() : this.renderLoader();
  }
}

Loader.propTypes = {
  withContainer: PropTypes.bool,
  visible: PropTypes.bool,
};

Loader.defaultProps = {
  withContainer: false,
  visible: false,
};

export default Loader;
