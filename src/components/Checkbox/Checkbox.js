import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './Checkbox.sass';

class Checkbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: props.checked,
    };

    // if (props.label) {
    //   this.uniqId = Math.random().toString(20).substring(10);
    // }
  }

  componentWillReceiveProps = (nextProps) => {
    this.setState({ checked: nextProps.checked });
  };

  onChange = () => {
    const { onChange, value } = this.props;
    onChange(this.state.checked, value);
  };

  getClass = () => classNames('checkbox', {
    'checkbox--invalid': !this.props.valid,
    'checkbox--disabled': this.props.disabled,
  });

  handleChange = (event) => {
    const checked = event.target.checked;
    this.setState({ checked }, this.onChange);
  };

  switchOff = () => {
    this.setState({ checked: false });
  };

  switchOn = () => {
    this.setState({ checked: true });
  };

  render() {
    const { name, label, disabled, value } = this.props;
    const { checked } = this.state;
    return (
      <div className={this.getClass()}>
        {label ?
          <label htmlFor={name}><span>{label}</span></label>
        : null}
        <input
          type="checkbox"
          checked={checked}
          onChange={this.handleChange}
          name={name}
          id={name}
          disabled={disabled}
          value={value}
        />
      </div>
    );
  }
}

Checkbox.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  checked: PropTypes.bool,
  label: PropTypes.string,
  valid: PropTypes.bool,
};
Checkbox.defaultProps = {
  onChange: () => true,
  disabled: false,
  checked: false,
  label: '',
  valid: true,
};

export default Checkbox;
