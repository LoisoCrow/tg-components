import React, { Component } from 'react';
import { Modal, Button, XmlWrapper } from 'components';
import { PropTypes } from 'prop-types';
import './SignModal.sass';

class SignModal extends Component {
  additionalInfo() {
    const { statementDescription } = this.props;
    if (statementDescription) {
      return (
        <div className="sign-modal__info">
          {statementDescription}
        </div>
      );
    }

    return null;
  }

  renderCertificate = () => {
    const { certificate } = this.props;

    if (!certificate) return null;

    return (<div>
      <h1>Сертификат:</h1>
      {Object.keys(certificate).map(entry => (
        <div className="sign-modal__certificate" key={entry}>
          <label htmlFor={entry} className="sign-modal__certificate__label">{entry} </label>
          <span id={entry} className="sign-modal__certificate__text">
            {typeof certificate[entry] === 'string' && certificate[entry]}
          </span>
        </div>
      ))}
    </div>);
  };

  renderXml = () => {
    const { xml } = this.props;

    if (!xml) return null;

    return (
      <div>
        <h1>XML:</h1>
        <XmlWrapper xmlDOM={xml} />
      </div>
    );
  };

  render() {
    const { closeModal, show, signAndSend, xml, title } = this.props;
    return (
      <Modal
        title={title}
        closeModal={closeModal}
        show={show}
        size="large"
      >
        <div className="sign-modal">
          {this.renderCertificate()}
          {this.additionalInfo()}
          {this.renderXml()}
          <div className="sign-modal__button-container">
            <Button onPress={closeModal}>Отмена</Button>
            <Button disabled={!xml} bgColor="orange" onPress={signAndSend}>Подписать и отправить</Button>
          </div>
        </div>
      </Modal>
    );
  }
}

SignModal.propTypes = {
  title: PropTypes.string,
  xml: PropTypes.string,
  show: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  signAndSend: PropTypes.func.isRequired,
  certificate: PropTypes.shape({
    entry: PropTypes.string,
  }).isRequired,
  statementDescription: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
};

SignModal.defaultProps = {
  title: 'Подписание документа',
  xml: '',
  show: false,
  closeModal: () => false,
  signAndSend: () => false,
  statementDescription: null,
};

export default SignModal;
