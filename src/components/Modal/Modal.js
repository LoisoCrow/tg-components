import React, { Component } from 'react';
import { createPortal } from 'react-dom';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { Header, Footer } from 'components';
import './Modal.sass';

class Modal extends Component {
  componentWillMount() {
    this.root = document.createElement('div');
    this.root.classList.add('modal-container');
    document.body.appendChild(this.root);

    document.body.style.overflow = 'hidden';
    this.root.addEventListener('click', this.handleClickOutside, false);
  }

  componentDidMount() {
    this.modal.classList.toggle('__opened');
    this.root.classList.toggle('__opened');
  }

  componentWillUnmount() {
    document.body.removeChild(this.root);
    document.body.style.overflow = 'auto';
  }

  getClassName = () => `Modal __${this.props.size} ${this.props.bsClass}`;

  handleClickOutside = (e) => {
    const component = this.modal;
    if (e.path.includes(component)) {
      return;
    }
    this.props.closeModal();
  };

  renderHeader = () => {
    const {
      title,
      size,
      closeModal,
    } = this.props;

    if (size === 'full') {
      return (
        <div className="Modal__header">
          <Header />
          <div className="Modal__sub-header">
            <div className="__title">
              <button
                className="icon icon__back_page link"
                onClick={closeModal}
              />
              <h1>{title}</h1>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="Modal__fx-header">
        <h1>{title}</h1>
        <button className="ico ico__close" onClick={closeModal} />
      </div>
    );
  };

  render() {
    const { children, contentStyle, bottomPanel } = this.props;

    return (
      createPortal(
        <div
          className={this.getClassName()}
          ref={(node) => {
            this.modal = node;
          }}
        >
          {this.renderHeader()}
          <div className="Modal__fields" style={contentStyle}>{children}</div>
          {bottomPanel ?
            <Footer>{bottomPanel}</Footer>
          : null}
        </div>,
        this.root,
      )
    );
  }
}

Modal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
    ])),
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  size: PropTypes.oneOf(['small', 'large', 'default', 'full']),
  title: PropTypes.string,
  contentStyle: PropTypes.object,
  bottomPanel: PropTypes.element,
  bsClass: PropTypes.string,
};
Modal.defaultProps = {
  size: 'default',
  show: false,
  title: 'Модальное окно',
  contentStyle: null,
  bottomPanel: null,
  bsClass: '',
};

export default withRouter(Modal);
