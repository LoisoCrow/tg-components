import React from 'react';
import { Link } from 'react-router-dom';
import { routeMap } from 'router';
import logo from 'theme/images/logo.png';
import './Header.sass';

const Header = () => (
  <header>
    <Link to={routeMap.home()}>
      <div className="header header__title">
        <img className="header header__title__logo" src={logo} alt="logo" />
        <div className="header header__title__text">
            Информационный ресурс <br />маркировки товаров из натурального меха
        </div>
      </div>
    </Link>
  </header>
);

export default Header;
