import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './Button.sass';

class Button extends Component {
  getClass = () => {
    const { icon, bgColor, className } = this.props;

    return classnames('button', `button--${bgColor}`, { 'button--icon': icon }, className);
  };

  render() {
    const { children, type, onPress, icon, disabled } = this.props;

    return (
      <button
        type={type}
        onClick={onPress}
        className={this.getClass()}
        disabled={disabled}
      >
        {icon && <i className={`icon icon__${icon}`} />}
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  bgColor: PropTypes.oneOf(['blue', 'orange', 'gray']),
  type: PropTypes.oneOf(['submit', 'button', 'reset']),
  onPress: PropTypes.func.isRequired,
  children: PropTypes.string,
  icon: PropTypes.string,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};
Button.defaultProps = {
  bgColor: 'blue',
  type: 'button',
  children: 'Отправить',
  icon: '',
  disabled: false,
  className: '',
};

export default Button;
