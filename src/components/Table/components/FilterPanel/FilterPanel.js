import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { drop, isEmpty, reduce, cloneDeep } from 'lodash';
import { countableEnding } from 'utils';
import { Button, Modal, Content } from 'components';
import classnames from 'classnames';
import './FilterPanel.sass';

/**
 * Компонент представляет собой:
 *  1) кнопку разворачивания фильтра;
 *  2) информацию о количестве найденных записей;
 *  3) поля для ввода фильтров;
 *  4) строка с кратким обзором отфильтрованных значений
 */
class FilterPanel extends Component {
  constructor(props) {
    super(props);
    const { collapsed, collapsible, filter } = props;

    this.state = {
      collapsed,
      collapsible,
      filter,
      showModal: false,
    };
  }

  componentWillMount() {
    // TODO: filter
    // this.setLabelsFromChildren();
  }

  getValues = () => this.state.filter;

  setLabelsFromChildren = () => {
    const { children: childrenFromProps } = this.props;
    const allLabels = [];

    const findLabelsRecursive = (children) => {
      React.Children.forEach(children, (child) => {
        if (child.props.filter) {
          allLabels.push({
            name: child.props.name,
            label: child.props.label,
          });
        } else if (child.props.children) {
          findLabelsRecursive(child.props.children);
        }
      });
    };

    findLabelsRecursive(childrenFromProps);

    this.setState({ allLabels });
  };

  getChildren(children) {
    const { filter } = this.state;

    return React.Children.map(children, (child) => {
      if (child.props.filter) {
        const { name } = child.props;
        return React.cloneElement(child, {
          onChange: value => this.handleFieldsOnChange(value, name),
          // TODO: hardcode!
          value: this.findValue(name, filter),
        });
      } else if (child.props.children) {
        return React.cloneElement(child, {
          children: this.getChildren(child.props.children),
        });
      }

      return child;
    });
  }

  findValue = (name, filter) => {
    const nameAsArray = name.split('.');
    let value = cloneDeep(filter);
    for (let i = 0; i < nameAsArray.length; i += 1) {
      if (value[nameAsArray[i]] || value[nameAsArray[i]] === 0) {
        value = value[nameAsArray[i]];
      } else {
        return '';
      }
    }

    return value;
  }

  additionalCloseModal = () => {
    this.setState({ showModal: false }, () => { this.handleOnReset(); });
  };

  handleOnApply = () => {
    const { onApplyPress } = this.props;
    const { filter } = this.state;

    const iterator = (accumulator, object, key) => {
      // TODO: i hate this method
      if (typeof object === 'number' && object) {
        return { ...accumulator, [key]: object };
      }
      if (isEmpty(object) && object !== 0) {
        return accumulator;
      }
      if (Object.hasOwnProperty.call(object, 'text') && isEmpty(object.text)) {
        return accumulator;
      }
      if (typeof object === 'object') {
        if (Array.isArray(object)) {
          return { ...accumulator, [key]: object };
        }
        const temporaryObject = reduce(object, iterator, {});
        return isEmpty(temporaryObject) ? accumulator : { ...accumulator, [key]: temporaryObject };
      }
      return { ...accumulator, [key]: object };
    };

    const objectWithoutEmptyProperties = reduce(
      filter,
      iterator,
      {},
    );
    this.setState({ showModal: false }, () => onApplyPress(objectWithoutEmptyProperties));
  };

  resetFilter = () => {
    this.props.onApplyPress({});
  };

  handleOnReset = () => {
    this.setState({
      filter: {},
    }, this.resetFilter);
  };

  buildFilterPiece = (value, nameAsArray, filter) => {
    const modifiedFilter = cloneDeep(filter);

    if (nameAsArray.length === 1) {
      modifiedFilter[nameAsArray[0]] = value;

      return modifiedFilter;
    } else if (nameAsArray.length === 0) {
      return null;
    }

    modifiedFilter[nameAsArray[0]] = this.buildFilterPiece(
      value,
      drop(nameAsArray, 1),
      (modifiedFilter && modifiedFilter[nameAsArray[0]]) ? modifiedFilter[nameAsArray[0]] : {},
    );

    return modifiedFilter;
  }

  handleFieldsOnChange = (value, name) => {
    const { filter } = this.state;

    // TODO: HARDCODE!!!
    const nameAsArray = name.split('.');

    this.setState({ filter: this.buildFilterPiece(value, nameAsArray, filter) });
  };

  renderTotalRecords = count => (
    count ? <h4 className="__count">{countableEnding(count, ['Найдена', 'Найдено', 'Найдено'])} <span className="deepSkyBlue">{count}</span> {countableEnding(count, ['запись', 'записи', 'записей'])}</h4> : null
  );

  renderFields = () => (
    <div className="filter_panel__filter-groups">
      {this.getChildren(this.props.children)}
    </div>
  );

  render() {
    const { count, title, rightComponent, replaceTable, clickOnReplaceTable } = this.props;
    const { showModal } = this.state;

    const panelCollapsedClass = classnames({
      panel__collapsed: true,
      filter_panel__full: true,
    });

    if (!showModal) {
      return (
        <Content className="filter_panel__collapsed-block">
          <div className="filter_panel__collapsed-block__left">
            <button
              className="icon icon__filter link"
              onClick={() => this.setState({ showModal: true })}
            />
            {this.renderTotalRecords(count)}
          </div>
          {/* <button
            className="icon icon__blue icon__close_refresh"
            onClick={clickOnReplaceTable} />
           */}
          {
            (rightComponent || replaceTable) &&
            <div className="filter_panel__collapsed-block__right">
              {replaceTable &&
                <button className="icon icon__blue icon__close_refresh" onClick={clickOnReplaceTable} />
              }
              {rightComponent && rightComponent}
            </div>
          }
        </Content>
      );
    }

    return (
      <Modal
        size="full"
        show={showModal}
        closeModal={() => this.setState({ showModal: false })}
        title={title}
      >
        <div className="filter_panel filter_panel__wrapper">
          <div className={panelCollapsedClass}>
            <Content>
              {this.renderTotalRecords(count)}
              {this.renderFields()}
            </Content>
            <div className="mark-modal__bottom-panel">
              <Content>
                <div className="__left">
                  <Button onPress={this.handleOnApply}>Загрузить и показать</Button>
                  <Button
                    onPress={this.additionalCloseModal}
                    type="reset"
                  >Сбросить настройки</Button>
                </div>
              </Content>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

FilterPanel.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
  collapsed: PropTypes.bool,
  collapsible: PropTypes.bool,
  count: PropTypes.number,
  filter: PropTypes.object,
  onApplyPress: PropTypes.func.isRequired,
  title: PropTypes.string,
  rightComponent: PropTypes.element,
  replaceTable: PropTypes.bool,
  clickOnReplaceTable: PropTypes.func,
};

FilterPanel.defaultProps = {
  count: 0,
  collapsible: true,
  collapsed: true,
  filter: {},
  onResetPress: null,
  title: 'Фильтр',
  rightComponent: null,
  replaceTable: false,
  clickOnReplaceTable: () => true,
};

export default FilterPanel;
