import { map } from 'lodash';

const usersProcesses = {
  1: 'в стадии реорганизации',
  2: 'в стадии ликвидации',
  3: 'в стадии банкротства',
  4: 'реорганизована',
  5: 'ликвидирована',
  6: 'банкрот',
};

export const filterOptions = map(usersProcesses, (name, value) => ({
  value: Number(value),
  name,
}));

export default usersProcesses;
