import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import './Pagination.sass';

class Pagination extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
    };
  }

  componentWillMount() {
    document.addEventListener('click', this.handleClickOutside, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  onEnter = (event) => {
    const {
      paginationLimit: limit,
      onPageChange,
    } = this.props;

    const value = (event.target.value * 1);
    const newOffset = (value - 1) * limit;

    if (event.key === 'Enter' && value >= 1 && value <= this.pagesCount()) {
      onPageChange({ offset: newOffset, limit });
    } else {
      return false;
    }
    return true;
  };

  nextPage = () => {
    const {
      paginationLimit: limit,
      paginationOffset: offset,
      count,
      onPageChange,
    } = this.props;

    const newOffset = offset + limit < count
      ? offset + limit
      : 0;

    onPageChange({ offset: newOffset, limit });
  };

  previousPage = () => {
    const {
      paginationLimit: limit,
      paginationOffset: offset,
      count,
      onPageChange,
    } = this.props;

    const newOffset = offset - limit >= 0
      ? (offset - limit)
      : count - (count % limit) - (count % limit ? 0 : limit);

    onPageChange({ offset: newOffset, limit });
  };

  currentPage = () => {
    const { paginationLimit: limit, paginationOffset: offset } = this.props;

    return ((offset - (offset % limit)) / limit) + 1;
  };

  handleClickOutside = (e) => {
    const selectedBlock = this.drop;

    if (!e.path.includes(selectedBlock)) {
      this.setState({ isFocused: false });
    }
  };

  pagesCount = () => {
    const { count, paginationLimit: limit } = this.props;

    return Math.ceil(count / limit);
  };

  render() {
    const { isFocused } = this.state;

    return (
      <div className="component-pagination">
        <div className="pagination">
          <div className="ico ico--back" onClick={this.previousPage} />
          <span>{`${this.currentPage()}/${this.pagesCount()}`}</span>
          <div className="ico ico--forward" onClick={this.nextPage} />
        </div>
        <div
          className="drop"
          onClick={() => this.setState({ isFocused: true })}
          ref={(c) => { this.drop = c; }}
        >
          <span>...</span>
          <div className={`field ${isFocused ? '' : 'hide'}`}>
            <input
              type="text"
              onBlur={() => this.setState({ isFocused: false })}
              onKeyPress={this.onEnter}
              placeholder="стр"
            />
            <span>
              ...
            </span>
          </div>
        </div>
      </div>
    );
  }
}

Pagination.propTypes = {
  count: PropTypes.number.isRequired,
  paginationLimit: PropTypes.number.isRequired, // != 0!
  paginationOffset: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
