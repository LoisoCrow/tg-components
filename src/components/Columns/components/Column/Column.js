import React from 'react';
import { PropTypes } from 'prop-types';
import './Column.sass';

const Column = ({ children, className, width, isColumn, ...otherProps }) => (
  <div className={`columns__column ${className}`} {...otherProps} style={{ width }}>
    {children}
  </div>
  );

Column.propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element,
  ]),
  isColumn: PropTypes.bool,
};

Column.defaultProps = {
  indicator: 'Column',
  className: '',
  width: null,
  children: null,
  isColumn: true,
};

export default Column;
