import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';
import anime from 'animejs';

class Dropdown extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount() {
    this._root = document.createElement('div');
    this._root.classList.add('DropdownMenuDropdown');
    const style = this._root.style;
    const rect = this.props.rect;

    style.left = `${rect.x}px`;
    style.top = `${rect.y}px`;

    document.body.appendChild(this._root);
    document.addEventListener('click', this.handleClickOutside, false);
    anime({
      targets: this._root,
      duration: 2000,
      opacity: 1,
    });
  }

  componentDidMount() {
  }

  componentWillUnmount() {
    document.body.removeChild(this._root);
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  onChange = (obj) => {
    this.props.onChange(obj);
  }

  handleClickOutside = (e) => {
    if (!e.path.includes(this._optionsList)) {
      this.props.disableFocus();
    }
  }

  renderOption = obj => (
    <div
      role="presentation"
      className="DropdownMenuDropdown__item"
      onClick={() => this.onChange(obj)}
      key={obj.text}
    >
      <span>{obj.text}</span>
    </div>
    )

  render() {
    const { options } = this.props;

    return (
      createPortal(
        <div ref={(node) => { this._optionsList = node; }}>
          {options.map(obj => this.renderOption(obj))}
        </div>
        , this._root,
      )
    );
  }
}

Dropdown.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
  })).isRequired,
  rect: PropTypes.shape({
    x: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    y: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }).isRequired,
  disableFocus: PropTypes.func.isRequired,
};

export default Dropdown;
