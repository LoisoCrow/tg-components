import createReducer from './createReducer';

const createLocalReducer = (constants, additionalActions = {}, additionalInitialState = {}) => {
  const { handlers } = constants;
  const actions = {};

  if (handlers.sort) {
    actions[constants.handlers.sort] = (state, action) => {
      const { nameAttribute: name, direction } = action.payload[0];

      return {
        ...state,
        query: { ...state.query, sort: action.payload },
        data: state.data.sort((a, b) => {
          const first = a[name] ? `${a[name]}` : '';
          const second = b[name] ? `${b[name]}` : '';

          const result = 1;
          if (direction) {
            if (first > second) {
              return -result;
            }
            return result;
          }

          if (first > second) {
            return result;
          }
          return -result;
        }),
      };
    };
  }

  if (handlers.delete) {
    actions[handlers.delete] = (state, action) => {
      const index = state.data.findIndex(row => row._id === action.payload);
      const data = [
        ...state.data.slice(0, index),
        ...state.data.slice(index + 1),
      ];

      return {
        ...state,
        data,
        count: data.length,
      };
    };
  }

  return createReducer(constants, { ...actions, ...additionalActions }, additionalInitialState);
};

export default createLocalReducer;
