const defaultSeparator = '*';

const getMaskedValue = (value, mask, customSeparator = '*') => {
  if (!mask) return value;
  const separator = customSeparator || defaultSeparator;
  const valueToMask = mask.split('');
  let j = 0;
  for (let i = 0; i < mask.length; i += 1) {
    if (mask[i] !== separator) {
      valueToMask[i] = mask[i];
    } else {
      if (value[j]) {
        valueToMask[i] = value[j];
      } else {
        valueToMask[i] = '';
      }
      if (j < (mask.length - 1)) j += 1;
    }
  }
  return valueToMask.join('');
};

export default getMaskedValue;
