export const mergeColumns = (columns, initialColumns) => initialColumns.map((column) => {
  const storeColumn = columns.find(x => x.field === column.field);
  if (storeColumn) {
    return { ...column, ...storeColumn };
  }

  return column;
});
