const defaultSeparator = '*';

const findValueEnd = (value, mask, customSeparator) => {
  if (!mask) return value.length;
  if (!value) return 0;
  const separator = customSeparator || defaultSeparator;
  let remainingValue = value.length;
  let scryPosition = 0;
  for (let i = 0; i < mask.length; i += 1) {
    scryPosition += 1;
    if (mask[i] === separator) {
      remainingValue -= 1;
      if (remainingValue === 0) break;
    }
  }
  return scryPosition;
};

export default findValueEnd;
