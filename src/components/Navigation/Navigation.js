import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import './Navigation.sass';

class Navigation extends Component {
  constructor(props) {
    super(props);

    const { selected, items, exact } = props;
    this.state = {
      selected,
      items: this.initItems(items, selected, exact),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selected !== this.state.selected) {
      const { selected, items, exact } = nextProps;
      this.setState({
        items: this.initItems(items, selected, exact),
        selected,
      });
    }
  }

  initItems = (items, selected, exact) => {
    let somethingWasSelected = false;
    const parsedItems = items.map((item) => {
      const updatedItem = {
        ...item,
        visible: item.visible || true,
        selected: exact ? item.link === selected : selected.indexOf(item.link) !== -1,
      };

      if (updatedItem.selected) {
        somethingWasSelected = true;
      }

      return updatedItem;
    });

    if (!somethingWasSelected) {
      parsedItems[0].selected = true;
    }

    return parsedItems;
  };

  renderTabHeader = (tab) => {
    const { subMenu } = this.props;
    const linkClassName = classnames('navigation__item', {
      'navigation__item-active': tab.selected,
      'navigation__item-sub': subMenu,
    });
    const itemClassName = classnames('navigation__items', {
      'navigation__items-active': tab.selected,
      'navigation__item-sub': subMenu,
    });
    return (
      <div key={tab.title} className={itemClassName}>
        <div>
          <Link to={tab.link} className={linkClassName}>{tab.title}</Link>
        </div>
      </div>
    );
  };

  render() {
    const { items } = this.state;
    const { subMenu } = this.props;
    const className = classnames('navigation__content', { 'navigation__content-submenu': subMenu });
    return (
      <div className="navigation">
        <div className={className}>
          {items.map(this.renderTabHeader)}
        </div>
      </div>
    );
  }

}

Navigation.propTypes = {
  selected: PropTypes.string,
  exact: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
    link: PropTypes.string,
  })).isRequired,
  subMenu: PropTypes.bool,
};

Navigation.defaultProps = {
  selected: '',
  exact: false,
  subMenu: false,
};

export default Navigation;
