import React from 'react';
import Modal from './Modal';

const ModalWrapper = props => (
  props.show ? <Modal {...props} /> : null
);

export default ModalWrapper;
