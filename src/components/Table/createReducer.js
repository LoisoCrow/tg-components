import tableState from 'store/tableState';

/**
 * @typedef {Object} Actions
 * @property {function} setPagination
 * @property {function} setFilter
 * @property {function} setSorting
 * @property {function} selectMember
 * @property {function} getData
 *
 * @typedef {Object} thunk
 * @property {string} request - redux action type
 * @property {string} success - redux action type
 * @property {string} error - redux action type
 *
 * @typedef {Object} handlers
 * @property {string} select - redux action type
 * @property {string} sort - redux action type
 * @property {string} page - redux action type
 * @property {string} filter - redux action type
 * @property {string} delete - redux action type
 * @property {string} columns - redux action type
 *
 * @typedef {Object} constants
 * @property {thunk}
 * @property {handlers}
 *
 * @typedef {Object.<string, function>} ActionMap
 *
 * example:
 *   'DATA_SUCCESS': (state, action) => ({
 *     ...state,
 *     loading: false,
 *     count: action.payload.count,
 *     data: action.payload.elements,
 *     error: '',
 *   }),
 *
 * @param {constants} constants - redux action types structure
 * @param {ActionMap} additionalActions
 *
 * @returns {function} reducer - redux reducer
*/

const createReducer = (constants, additionalActions = {}, additionalInitialState = {}) => {
  const { thunk = {}, handlers } = constants;

  const handlerMap = {};

  if (thunk) {
    if (thunk.request) {
      handlerMap[thunk.request] = state => ({
        ...state,
        loading: true,
      });
    }

    if (thunk.success) {
      handlerMap[thunk.success] = (state, action) => ({
        ...state,
        loading: false,
        count: action.payload.count,
        data: action.payload.elements,
        error: '',
      });
    }

    if (thunk.error) {
      handlerMap[thunk.error] = (state, action) => ({
        ...state,
        loading: false,
        error: action.payload,
      });
    }
  }

  if (handlers) {
    if (handlers.sort) {
      handlerMap[handlers.sort] = (state, action) => ({
        ...state,
        query: { ...state.query, sort: action.payload },
      });
    }
    if (handlers.filter) {
      handlerMap[handlers.filter] = (state, action) => ({
        ...state,
        query: { ...state.query, filter: action.payload, offset: 0 },
      });
    }
    if (handlers.page) {
      handlerMap[handlers.page] = (state, action) => ({
        ...state,
        query: { ...state.query, ...action.payload },
      });
    }
    if (handlers.select) {
      handlerMap[handlers.select] = (state, action) => ({
        ...state,
        selected: action.payload,
      });
    }
    if (handlers.columns) {
      handlerMap[handlers.columns] = (state, action) => ({
        ...state,
        columns: action.payload,
      });
    }
    if (handlers.scroll) {
      handlerMap[handlers.scroll] = (state, action) => ({
        ...state,
        scroll: action.payload,
      });
    }
  }

  Object.assign(handlerMap, additionalActions);

  const create = (initialState, map) => (state = initialState, action) => {
    if (Object.hasOwnProperty.call(map, action.type)) {
      return map[action.type](state, action);
    }
    return state;
  };

  return create({ ...tableState, ...additionalInitialState }, handlerMap);
};

export default createReducer;
