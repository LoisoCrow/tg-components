import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';

export const withQuery = Table => class extends Component {
  static propTypes = {
    query: PropTypes.object,
    getData: PropTypes.func,
  }

  static defaultProps = {
    query: {},
    getData: () => true,
  }

  componentWillMount() {
    this.updateTable(this.props.query);
  }

  componentWillReceiveProps(nextProps) {
    if (isEqual(this.props.query, nextProps.query)) {
      return;
    }

    this.updateTable(nextProps.query);
  }

  updateTable = (query) => {
    const { getData } = this.props;

    if (getData) {
      getData(query);
    }
  };

  render() {
    return (
      <Table {...this.props} />
    );
  }
};
