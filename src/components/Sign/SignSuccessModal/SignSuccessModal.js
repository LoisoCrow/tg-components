import React from 'react';
import { Modal, Button } from 'components';
import { PropTypes } from 'prop-types';
import './SignSuccessModal.sass';

const SignSuccessModal = ({ closeModal, show, title }) => (
  <Modal
    title={title}
    closeModal={closeModal}
    show={show}
    size="small"
  >
    <div className="success-modal">
      <div className="success-modal__body">
        Заявление успешно отправлено
      </div>
      <div className="success-modal__button-container">
        <Button onPress={closeModal}>Готово</Button>
      </div>
    </div>
  </Modal>
  );

SignSuccessModal.propTypes = {
  title: PropTypes.string,
  show: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
};

SignSuccessModal.defaultProps = {
  title: '',
  xml: '',
  show: false,
  closeModal: () => false,
};

export default SignSuccessModal;
