export const routeMap = {
  home: () => '/home',
  setting_ecp: () => '/',
  login: () => '/login',
  users: (options = {}) => {
    let url = '/users/register';
    if (options.child) {
      url = `${url}/${options.child}`;
    }
    return url;
  },
  requests: (options = {}) => {
    let url = '/requests';
    if (options.child) {
      url = `${url}/${options.child}`;
    }
    return url;
  },
  // kiz
  kiz: (options = {}) => {
    let url = '/kiz';
    if (options.child) {
      url = `${url}/${options.child}`;
    }
    if (options.modal) {
      url = `${url}${options.modal}`;
    }
    return url;
  },
  regStatus: () => '/reg/status',
  authStatus: () => '/auth/status',
  kizOrders: () => '/kiz/orders',
  kizWaiting: () => '/kiz/waiting',

  profile: () => '/profile',
  products: (options = {}) => {
    const url = '/products';
    if (options.child) {
      return `${url}/${options.child}`;
    }
    return url;
  },
  commitWaitingKiz: () => '/commitWaitingKiz',
  transport: () => '/transport',
  select: () => '/select',
  registration: (options = {}) => `/registration${options.child ? `/${options.child}` : ''}`,
  settings: () => '/settings',
  actions: () => '/actions',
  help: () => '/help',
  ui: (options = {}) => {
    const url = '/ui';
    if (options.modal) {
      return `${url}${options.modal}`;
    }

    if (options.child) {
      return `${url}/${options.child}`;
    }

    return url;
  },
};
