import React from 'react';
// import { PropTypes } from 'prop-types';
import Loader from '../Loader/Loader';
import { TableComponent, PaginationPanel, FilterPanel } from './components';
import { mergeColumns } from './utils';

const renderFilterPanel = (props) => {
  const {
    filterContent,
    rightComponent = null,
    query,
    count,
    setFilter,
  } = props;
  if (!filterContent) {
    return null;
  }
  let connectedComponent = null;
  const { filter } = query;
  if (rightComponent) {
    const { name } = rightComponent.props;
    connectedComponent = React.cloneElement(rightComponent, {
      onChange: value => setFilter({
        ...filter,
        [name]: value,
      }),
      value: (filter[name] || filter[name] === 0) ? filter[name] : '',
    });
  }
  return (
    <FilterPanel
      onApplyPress={setFilter}
      count={count}
      filter={filter}
      rightComponent={connectedComponent}
    >
      {filterContent()}
    </FilterPanel>
  );
};

const Table = (props) => {
  const {
    commonSelected,
    selected,
    columns,
    initialColumns,
    query,
    loading,
    data,
    count,
    localPagination,
  } = props;
  const newData = localPagination ? data.slice(query.offset, query.offset + query.limit) : data;
  const newCount = localPagination ? data.length : count;
  const allSelected = commonSelected || selected || [];
  return (
  // TODO: вынести с сасс
    <div className="table" style={{ position: 'relative' }}>
      {renderFilterPanel(props)}
      <TableComponent
        {...props}
        count={newCount}
        data={newData}
        columns={mergeColumns(columns, initialColumns)}
        sort={query && query.sort}
        selected={allSelected}
      />
      <PaginationPanel
        {...props}
        paginationLimit={query && query.limit}
        paginationOffset={query && query.offset}
        selectedLength={allSelected.length}
      />
      <Loader visible={loading} withContainer />
    </div>
);
};

Table.propTypes = {
//   count: PropTypes.number.isRequired,
//   filteredCount: PropTypes.number,
//   data: PropTypes.array.isRequired,
//   selected: PropTypes.array,
//   query: PropTypes.shape({
//     limit: PropTypes.number.isRequired,
//     offset: PropTypes.number.isRequired,
//     sort: PropTypes.arrayOf(PropTypes.object),
//   }).isRequired,
//   handlers: PropTypes.shape({
//     onSelect: PropTypes.func,
//     onSort: PropTypes.func,
//     onPageChange: PropTypes.func,
//     onFilter: PropTypes.func,
//     onRowDelete: PropTypes.func,
//   }).isRequired,
//   actions: PropTypes.shape({
//     first: PropTypes.element,
//     second: PropTypes.element,
//     others: PropTypes.arrayOf(PropTypes.element),
//   }),
//   columns: PropTypes.arrayOf(PropTypes.shape({
//     header: PropTypes.string.isRequired,
//     field: PropTypes.string.isRequired,
//     hidden: PropTypes.bool,
//     sorting: PropTypes.bool,
//     width: PropTypes.number.isRequired,
//   })).isRequired,
//   options: PropTypes.shape({
//     multiselect: PropTypes.bool,
//     toggleColumns: PropTypes.bool,
//     selectColumn: PropTypes.bool,
//     rowCanBeDeleted: PropTypes.bool,
//   }),
//   pagination: PropTypes.bool,
//   loading: PropTypes.bool,
//   disable: PropTypes.func,
};

Table.defaultProps = {
  options: {
    toggleColumns: true,
    selectColumn: true,
    multiselect: true,
    rowCanBeDeleted: false,
    disabledRows: true,
  },
  buttons: [],
  actions: {},
  filteredCount: -1,
  paginationProps: {
    items: [1, 2, 10, 50, 100, 1000],
    countFiltered: -1,
  },
  selected: [],
  handlers: {},
  loading: false,
  pagination: true,
  disable: () => false,
};

export default Table;
