import { connect } from 'react-redux';
import SignModal from './SignModal';
import mapCertificate from './mapCertificate';

const mapStateToProps = state => ({
  certificate: mapCertificate(state.user.certificate),
});

export default connect(mapStateToProps)(SignModal);
