import PropTypes from 'prop-types';
import { toDefaultFormat } from 'utils';

const DateField = ({ value }) => {
  if (value) {
    const date = toDefaultFormat(value);
    return (date === 'Invalid date') ? '' : date;
  }
  return '';
};

DateField.propTypes = {
  value: PropTypes.string.isRequired,
};

export default DateField;
