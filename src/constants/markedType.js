import map from 'lodash/map';

export const NEW_PRODUCT = 0;
export const FROM_INDIVIDUAL = 1;
export const RETURN_DAMAGED = 2;

const markedType = {
  [NEW_PRODUCT]: 'Новый товар',
  [FROM_INDIVIDUAL]: 'Принят от ФЛ по договору комиссии',
  [RETURN_DAMAGED]: 'Возвращенный товар с поврежденным КиЗ',
};

export const filterOptions = map(markedType, (name, value) => ({
  value: parseInt(value, 10),
  name,
}));

export default markedType;
