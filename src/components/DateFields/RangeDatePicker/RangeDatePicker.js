import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import anime from 'animejs';
import momentPropTypes from 'react-moment-proptypes';
import omit from 'lodash/omit';
import { toServerFormat } from 'utils';
import { DateRangePicker } from 'react-dates';
import { moment } from 'utils/moment';
import './../default.css';
import './../DateFields.sass';


class RangeDatePicker extends Component {
  constructor(props) {
    super(props);

    let focusedInputPicker = null;

    if (props.autoFocus) {
      focusedInputPicker = props.startDateId;
    } else if (props.autoFocusEndDate) {
      focusedInputPicker = props.endDateId;
    }

    this.state = {
      focusedComponent: false,
      focusedInputPicker,
      startDate: props.initialStartDate,
      endDate: props.initialEndDate,
      month: moment(props.initialStartDate).isValid() ? moment(props.initialStartDate).month()
        : 1,
      year: moment().year(),
    };

    this.formatDate = 'YYYY-MM-DD';
  }

  componentWillMount() {
    const startDate = moment(this.props.startDate);
    const endDate = moment(this.props.endDate);

    this.setState({
      startDate: startDate.isValid() ? startDate : null,
      endDate: endDate.isValid() ? endDate : null,
    });

    document.addEventListener('click', this.handleClickOutside, false);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.focusedInputPicker === null && this.state.focusedComponent) {
      setTimeout(() => {
        this.setState({ focusedComponent: false });
      }, 20);
    }
    return true;
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  onDatesChange = ({ startDate, endDate }) => {
    const { onChange, name } = this.props;

    this.setState({ startDate, endDate }, () => {
      const { startDate: start, endDate: end } = this.state;

      onChange({
        name,
        value: {
          startDate: toServerFormat(start),
          endDate: toServerFormat(end),
        },
      });
    });

    // if (moment(this.state.endDate).format() !== moment(endDate).format()) {
    //   this.setState({ focusedComponent: false });
    // }
  };

  onFocusChange = (focusedInputPicker) => {
    this.setState({ focusedInputPicker });
  };

  getValue = () => {
    const { onChange, name } = this.props;
    const { startDate, endDate } = this.state;

    onChange({
      name,
      value: {
        startDate: moment(startDate).isValid() ?
          moment(startDate).format(this.formatDate) : '',
        endDate: moment(endDate).isValid() ?
          moment(endDate).format(this.formatDate) : '',
      },
    });
  };

  getClassInput = () => classNames('RangeDatePicker__input', {
    RangeDatePicker__input__error: this.state.error,
    // RangeDatePicker__input__focus: this.state.focusedComponent,
    RangeDatePicker__input__focus: this.state.focusedInputPicker,
  });

  getClassPicker = () => classNames('RangeDatePicker__calendar', {
    // hide: !this.state.focusedComponent,
    // hide: !this.state.focusedInputPicker,
  });

  toggleMonth = (direction) => {
    const { month, year } = this.state;

    if ((month === 1 || !month) && direction === 'prev') {
      this.setState({ month: 12, year: year - 1 });
    } else if (month === 12 && direction === 'next') {
      this.setState({ month: 1, year: year + 1 });
    } else if (direction === 'prev') {
      this.setState({ month: month - 1 });
    } else if (direction === 'next') {
      this.setState({ month: month + 1 });
    }
  };

  handleClickOutside = (e) => {
    const { startDate, endDate } = this.state;

    if (!e.path.includes(this.parentComponent)) {
      this.onFocusChange(null);

      if (startDate && !endDate) {
        this.setState({ endDate: moment(startDate).add(1, 'days') });
      } else if (!startDate && endDate) {
        this.setState({ startDate: moment(endDate).subtract(1, 'days') });
      }
    }
  };

  resetValue = () => {
    const { initialStartDate, initialEndDate } = this.props;

    this.onDatesChange({ startDate: initialStartDate, endDate: initialEndDate });
    this.onFocusChange(null);
  };

  handleClickInput = () => {
    const { focusedComponent } = this.state;

    this.setState({ focusedComponent: !focusedComponent }, () => {
      if (!focusedComponent) {
        this.onFocusChange('startDate');
        anime({
          targets: this.calendar,
          duration: 2000,
          opacity: 1,
        });
      } else {
        this.onFocusChange(null);
      }
    });
  };

  selectedMonth = () => {
    const { month, year } = this.state;
    const { onChange, name, isOutsideRange } = this.props;

    const endDay = moment(`${year}-${!month ? '01' : month}`, 'YYYY-MM').daysInMonth();

    const startDate = moment(`${year}-${!month ? '01' : month}-01`);
    let endDate = moment(`${year}-${!month ? '01' : month}-${endDay}`);

    if (isOutsideRange(endDate)) {
      endDate = moment();
    }

    if (!isOutsideRange(startDate)) {
      this.onDatesChange({ startDate, endDate });
      onChange({
        name,
        value: {
          startDate: startDate.format(this.formatDate),
          endDate: endDate.format(this.formatDate),
        },
      });
    }

    this.setState({ focusedComponent: false }, () => { this.onFocusChange(null); });
  };

  render() {
    const { focusedInputPicker, startDate, endDate, focusedComponent } = this.state;
    const { label } = this.props;
    const props = omit(this.props, [
      'autoFocus',
      'autoFocusEndDate',
      'initialStartDate',
      'initialEndDate',
      'label',
      'name',
      'onChange',
      'withFullScreen',
      'error',
      'startDate',
      'endDate',
      'filter',
      'value',
    ]);

    let title;
    if (startDate && endDate) {
      // TODO: Hardcode!!!
      try {
        const titleStart = startDate.format('DD.MM.YYYY');
        const titleEnd = endDate.format('DD.MM.YYYY');

        title = `с ${titleStart} по ${titleEnd}`;
      } catch (exception) {
        title = null;
      }
    }

    return (
      <div className="RangeDatePicker" ref={(node) => { this.parentComponent = node; }}>
        <div
          className={this.getClassInput()}
          onClick={this.handleClickInput}
          role="presentation"
        >
          <h4>{label}</h4>
          {!focusedComponent ?
            <div className="RangeDatePicker__input__content">
              <span>{title || 'Укажите период'}</span>
              <i className="icon icon__calendar" />
            </div> : null
          }
        </div>
        {focusedComponent ?
          <div className={this.getClassPicker()} ref={(node) => { this.calendar = node; }}>
            <DateRangePicker
              {...props}
              onDatesChange={this.onDatesChange}
              onPrevMonthClick={() => this.toggleMonth('prev')}
              onNextMonthClick={() => this.toggleMonth('next')}
              onFocusChange={this.onFocusChange}
              focusedInput={focusedInputPicker}
              startDate={startDate}
              endDate={endDate}
            />
            <div className="RangeDatePicker__btn">
              <button
                className="RangeDatePicker__btn__selected"
                onClick={this.selectedMonth}
              >
                За весь месяц
              </button>
              <button
                className="RangeDatePicker__btn__reset"
                onClick={this.resetValue}
              >
                Сбросить
              </button>
            </div>
          </div>
        : null}
      </div>
    );
  }
}

RangeDatePicker.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  startDatePlaceholderText: PropTypes.string,
  endDatePlaceholderText: PropTypes.string,
  initialStartDate: momentPropTypes.momentObj,
  initialEndDate: momentPropTypes.momentObj,
  disabled: PropTypes.bool,
  numberOfMonths: PropTypes.number,
  error: PropTypes.string,
  isOutsideRange: PropTypes.func,
};
RangeDatePicker.defaultProps = {
  startDatePlaceholderText: 'С',
  endDatePlaceholderText: 'По',
  initialStartDate: null,
  initialEndDate: null,
  disabled: false,
  error: '',
  hideKeyboardShortcutsPanel: true,

  // input related props
  startDateId: 'startDate',
  endDateId: 'endDate',
  showClearDates: false, // ?
  showDefaultInputIcon: false,
  customInputIcon: null,
  customArrowIcon: null,
  customCloseIcon: null,

  // calendar presentation and interaction related props
  numberOfMonths: 1, // ?
  renderMonth: null,
  orientation: 'horizontal',
  anchorDirection: 'left',
  horizontalMargin: 0,
  withPortal: false,
  withFullScreen: false,
  initialVisibleMonth: null,
  keepOpenOnDateSelect: false,
  reopenPickerOnClearDates: false,
  isRTL: false,

  // navigation related props
  navPrev: <i className="icon icon__arrow-left" />,
  navNext: <i className="icon icon__arrow-right" />,
  onPrevMonthClick() {},
  onNextMonthClick() {},
  onClose() {},

  // day presentation and interaction related props
  minimumNights: 1,
  enableOutsideDays: false,
  isDayBlocked: () => false,
  isDayHighlighted: () => false,
  isOutsideRange: value => value > moment(),

  // internationalization
  displayFormat: () => moment.localeData().longDateFormat('L'),
  monthFormat: 'MMMM YYYY',
};

export default RangeDatePicker;
