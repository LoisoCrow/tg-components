import moment from 'moment';
import 'moment/locale/ru';
import config from 'config';

const { serverFormat, defaultFormat } = config;

moment.locale('ru');

const format = (dateString, fromFormat, toFormat) => {
  const result = moment(dateString, fromFormat);

  return result.isValid() ? result.format(toFormat) : '';
};

export const toDefaultFormat = dateString => format(dateString, serverFormat, defaultFormat);

export const toServerFormat = dateString => format(dateString, defaultFormat, serverFormat);

export { moment };

