import map from 'lodash/map';

export const rejectReasons = {
  1: 'Порча',
  2: 'Уничтожение',
  3: 'Утеря',
  4: 'Ошибка в описании',
  5: 'Неактуален или повежден',
};

export const writeOffReasons = {
  6: 'Розничная продажа',
  7: 'Экспорт',
  8: 'Трансграичная торговля',
  9: 'Безвозмездная передача физлицу',
  10: 'Возврат физ лицу по договору комиссии',
  11: 'Договор рассрочки',
  12: 'Передача в связис банкротством',
  13: 'Передача в связи с ликвидацией',
  14: 'Передача конфискованного товара',
  15: 'Уничтожение товара',
};

export const allRejectionReasons = Object.assign({}, rejectReasons, writeOffReasons);

export const filterOptions = map(allRejectionReasons, (name, value) => ({
  value: parseInt(value, 10),
  name,
}));
