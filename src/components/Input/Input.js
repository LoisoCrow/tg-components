import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import classNames from 'classnames';
import InputMask from 'react-input-mask';
import './Input.sass';

// TODO: надо вывести под инпутом ошибку если она есть
// TODO: протестировать с ошибкой (визуально и снапшотно)
class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value || '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.lastValue) {
      this.setState({ value: nextProps.value });
    }
  }

  onChange = (event) => {
    const value = event.target.value;

    this.setState({ value });
    this.props.onChange(value);
  };

  getClass = () => (
    classNames({ 'field__input-error': this.props.error })
  )

  renderInput = () => {
    const { disabled, name, mask, label, value, onChange, error, ...otherProps } = this.props;
    return (
      <InputMask
        maskChar=""
        mask={mask}
        type="text"
        value={this.state.value}
        className={this.getClass()}
        onChange={this.onChange}
        disabled={disabled}
        name={name}
        id={name}
        {...otherProps}
      />
    );
  }

  renderError = () => {
    const { error } = this.props;

    if (!error) {
      return null;
    }

    return <span className="field__error">{error}</span>;
  };

  render() {
    const { label, name, required, subLabel } = this.props;

    return (
      <div className="field__input">
        <label htmlFor={name}>
          <h4>{label} {required && <span className="ico__required">*</span>} {subLabel && <span className="Input__subLabel">{subLabel}</span>}</h4>
          {this.renderInput()}
        </label>
        {this.renderError()}
      </div>
    );
  }
}

Input.propTypes = {
  value: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  mask: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  required: PropTypes.bool,
  subLabel: PropTypes.string,
};

Input.defaultProps = {
  value: '',
  disabled: false,
  mask: false,
  onChange: () => true,
  label: '',
  error: null,
  required: false,
  subLabel: '',
};

export default Input;
