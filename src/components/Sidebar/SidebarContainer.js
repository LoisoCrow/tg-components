import { connect } from 'react-redux';
import Sidebar from './Sidebar';

const mapStateToProps = (state) => {
  let user;
  const cert = state.user.certificate;
  const userData = state.user.data;
  if (Object.keys(userData).length) {
    user = {
      name: userData.fullName,
      position: cert ? cert.title : null,
    };
  }
  return {
    isAuthorized: state.user.isAuthorized,
    user,
    organizationName: cert ? cert.commonName : null,
  };
};

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(() => {}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
