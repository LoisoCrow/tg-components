import React from 'react';
import PropTypes from 'prop-types';
import './FilterGroup.sass';

const FilterGroup = ({ children, title }) => (
  <div className="filter-group">
    <div className="filter-group__header">
      <h3>{title}</h3>
    </div>
    <div className="filter-group__fields">
      {children}
    </div>
  </div>
);

FilterGroup.propTypes = {
  title: PropTypes.string,
};
FilterGroup.defaultProps = {
  title: '',
};

export default FilterGroup;
