import React, { Component } from 'react';
import { SingleDatePicker } from 'react-dates';
import anime from 'animejs';
import omit from 'lodash/omit';
import PropTypes from 'prop-types';
import { moment } from 'utils';
import momentPropTypes from 'react-moment-proptypes';
import classNames from 'classnames';
import './../default.css';
import './../DateFields.sass';

class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.convertValueToMoment(props.value),
      focused: false,
    };
  }

  componentWillMount() {
    document.addEventListener('click', this.handleClickOutside, false);
  }

  componentWillReceiveProps(nextProps) {
    const { value } = this.props;
    if (nextProps.value !== value) {
      this.setState({ value: this.convertValueToMoment(nextProps.value) });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  getClassInput = () => classNames('DateSinglePicker__input', {
    DateSinglePicker__input__error: this.props.error,
    // DateSinglePicker__input__focus: this.state.focusedComponent,
    DateSinglePicker__input__focus: this.state.focused,
  });

  getClassPicker = () => classNames('DateSinglePicker__calendar', {
    // hide: !this.state.focusedComponent,
    // hide: !this.state.focusedInputPicker,
  });

  convertValueToMoment = (value) => {
    if (value && !moment.isMoment(value)) {
      return moment(value);
    }

    return value;
  }

  handleChange = (value) => {
    const { onChange, format } = this.props;
    const formattedValue = moment(value).format(format);
    this.setState({ value, focused: false }, () => { onChange(value, formattedValue); });
  };


  handleFocusChange = () => {
    // todo:
    const { onFocus } = this.props;
    this.setState({ focused: true });

    anime({
      targets: this.calendar,
      duration: 2000,
      opacity: 1,
    });

    if (onFocus) {
      onFocus();
    }
  };

  handleClickOutside = (e) => {
    if (!e.path.includes(this.parentComponent)) {
      const { onBlur } = this.props;
      const { focused } = this.state;

      if (focused) {
        this.setState({ focused: false });
        if (onBlur) {
          onBlur();
        }
      }
    }
  };

  resetValue = () => {
    const { defaultValue } = this.props;

    this.handleChange(defaultValue);
  };

  renderError = () => {
    const { error } = this.props;

    if (!error) {
      return null;
    }

    return <span className="field__error">{error}</span>;
  };

  render() {
    const { label, placeholder, format } = this.props;
    const { value, focused } = this.state;

    const props = omit(this.props, [
      'autoFocus',
      'defaultValue',
      'label',
      'name',
      'onChange',
      'withFullScreen',
      'error',
      'validate',
      'form',
      'format',
      'value',
      'reopenPickerOnClearDates',
      'minimumNights',
      'onBlur',
      'onFocus',
      'parse',
      'format',
    ]);
    return (
      <div className="DateSinglePicker" ref={(node) => { this.parentComponent = node; }}>
        <div
          className={this.getClassInput()}
          onClick={this.handleFocusChange}
          role="presentation"
        >
          <h4>{label}</h4>
          {!focused &&
          <div className="DateSinglePicker__input__content">
            <span>{value ? value.format(format) : placeholder}</span>
            <i className="icon icon__calendar" />
          </div>
          }
          {this.renderError()}
        </div>
        {focused &&
        <div>
          <div className={this.getClassPicker()} ref={(node) => { this.calendar = node; }}>
            <SingleDatePicker
              placeholder={placeholder}
              numberOfMonths={1}
              date={value || null}
              onDateChange={this.handleChange}
              focused={focused}
              onFocusChange={this.handleFocusChange}
              {...props}
            />
            <div className="DateSinglePicker__btn">
              <button
                className="DateSinglePicker__btn__reset"
                onClick={this.resetValue}
              >
                  Сбросить
              </button>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

DatePicker.propTypes = {
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  defaultValue: momentPropTypes.momentObj,
  value: PropTypes.string,
  validate: PropTypes.func, // returns true or error string
  onChange: PropTypes.func,
  format: PropTypes.string,
  error: PropTypes.string,
};

DatePicker.defaultProps = {
  format: 'DD.MM.YYYY',
  placeholder: 'ДД.ММ.ГГГГ',
  error: null,
  defaultValue: null,
  value: null,
  label: null,
  onChange: () => false,
  validate: () => true,
  disabled: false,

  hideKeyboardShortcutsPanel: true,
  navPrev: <i className="icon icon__arrow-left" />,
  navNext: <i className="icon icon__arrow-right" />,
  onPrevMonthClick() {},
  onNextMonthClick() {},
  onClose() {},

  // calendar presentation and interaction related props
  numberOfMonths: 1, // ?
  renderMonth: null,
  orientation: 'horizontal',
  anchorDirection: 'left',
  horizontalMargin: 0,
  withPortal: false,
  withFullScreen: false,
  initialVisibleMonth: null,
  keepOpenOnDateSelect: false,
  reopenPickerOnClearDates: false,
  isRTL: false,

  // day presentation and interaction related props
  minimumNights: 1,
  enableOutsideDays: false,
  isDayBlocked: () => false,
  isDayHighlighted: () => false,
  isOutsideRange: value => value > moment(),
};

export default DatePicker;
