const getOffsetRect = (elm) => {
  const box = elm.getBoundingClientRect();
  const body = document.body;
  const docElm = document.documentElement;
  const scrollTop = window.pageYOffset || docElm.scrollTop || body.scrollTop;
  const scrollLeft = window.pageXOffset || docElm.scrollLeft || body.scrollLeft;
  const clientTop = docElm.clientTop || body.clientTop || 0;
  const clientLeft = docElm.clientLeft || body.clientLeft || 0;
  const top = (box.top + scrollTop) - clientTop;
  const left = (box.left + scrollLeft) - clientLeft;

  return { top: Math.round(top), left: Math.round(left) };
};

export default getOffsetRect;
