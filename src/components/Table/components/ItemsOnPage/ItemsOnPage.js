import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './ItemsOnPage.sass';

class ItemsOnPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      value: props.value,
    };
  }

  componentWillMount() {
    document.addEventListener('click', this.handleClickOutside, false);
  }

  // componentWillUpdate(nextProps, nextState) {
  //   if (JSON.stringify(this.state.value) !== JSON.stringify(nextState.value)) {
  //     this.props.onChange(nextState.value);
  //   }
  // }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, false);
  }

  getClass = value => (
    this.state.value === value ? '__active' : ''
  );

  handleClickOutside = (e) => {
    const selectedBlock = this.block;

    if (!e.path.includes(selectedBlock)) {
      this.setState({ isFocused: false });
    }
  };

  handleSelected = (value) => {
    this.setState(
      { value, isFocused: false },
      () => this.props.onChange({ offset: 0, limit: value }),
    );
  };

  render() {
    const { items } = this.props;
    const { isFocused, value } = this.state;

    return (
      <div
        className="component-items_on_page"
        ref={(c) => {
          this.block = c;
        }}
      >
        <div
          role="presentation"
          className="presentation static"
          onClick={() => this.setState({ isFocused: !isFocused })}
        >
          <span className="label">
            {!isFocused ? <span>Показать по <span className="docsCount">{value}</span></span> : 'Скрыть'}
          </span>
          <i className={`icon icon__arrow-down ${!isFocused ? '' : 'active'}`} />
        </div>
        <div className={`focus ${!isFocused ? 'inVisible' : ''}`}>
          <ul className="ul">
            {items.map(i => (
              <li className={`li ${this.getClass(i)}`} key={i} onClick={() => this.handleSelected(i)}>
                {i}
              </li>
            ))}
          </ul>
          <i className="icon icon__mark_lighter" />
        </div>
      </div>
    );
  }
}

ItemsOnPage.propTypes = {
  items: PropTypes.arrayOf(PropTypes.number),
  value: PropTypes.number,
  onChange: PropTypes.func,
};
ItemsOnPage.defaultProps = {
  items: [10, 50, 100, 1000],
  value: 100,
  onChange: () => true,
};

export default ItemsOnPage;
