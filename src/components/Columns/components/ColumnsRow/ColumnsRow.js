import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { ColumnsGroup } from 'components';
import './ColumnsRow.sass';

class ColumnsRow extends Component {
  getChild(child) {
    if (child.props.indicator === 'ColumnsGroup') {
      return this.getGroup(child);
    }

    if (child.props.indicator === 'Column') {
      return this.getColumn(child);
    }
    return child;
  }

  // TODO: разобраться как тут все считается
  // есть подозрение что тут где-то спрятана ошибка
  getGroup(child) {
    const { columnsCount } = this.props;
    const children = child.props.children.filter(propsChild => propsChild);
    return (
    React.cloneElement(
      child,
      {
        children: React.Children.map(children, column => React.cloneElement(
          column,
          {
            children: column.props.children,
            width: column.props.width || `${100 / children.length}%`,
          },
        )),
        width: `${(100 / columnsCount) * children.length}%`,
      },
    )
    );
  }

  getColumn(column) {
    const { columnsCount, children } = this.props;

    return (
      <ColumnsGroup width={`${100 / columnsCount || children.length}%`}>
        {
              React.cloneElement(
                column,
                {
                  children: column.props.children,
                  width: column.props.width || `${100}%`,
                },
              )
          }
      </ColumnsGroup>
    );
  }

  render() {
    const { className, children } = this.props;
    return (
      <div className={`columns__row ${className}`}>
        {React.Children.map(children, child => this.getChild(child))}
      </div>
    );
  }
}

ColumnsRow.propTypes = {
  className: PropTypes.string,
  columnsCount: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element,
  ]),
};

ColumnsRow.defaultProps = {
  indicator: 'ColumnsRow',
  className: '',
  columnsCount: null,
  children: null,
  isColumnsRow: true,
};

// Пример использования:
/* <Columns>
     <ColumnsGroup title="Акт списания">
      <Column>
        <Input label="Номер документа" name="reject_doc_num" form />
      </Column>
       <Column>
        <SingleDatePicker label="Дата выдачи документа" name="reject_doc_date" form />
       </Column>
     </ColumnsGroup>
     <ColumnsGroup title="Акт списания">
      <Column>
        <Input label="Номер документа" name="reject_doc_num" form />
      </Column>
     </ColumnsGroup>
</Columns> */

export default ColumnsRow;
