import React from 'react';
import { Select } from 'components';
import { PropTypes } from 'prop-types';

const values = {
  1: 'Знак повреждён',
  2: 'Знак утерян',
  3: 'Знак уничтожен',
  5: 'Ошибка в описании',
  6: 'Розничная продажа',
};

const RejectReason = (props) => {
  if (props.editable) {
    return (
      <Select
        name="reject_reason"
        value={props.row.reject.reason}
        onChange={props.onChange(props.row)}
        options={Object.keys(values).map(key => ({ value: key, name: values[key] }))}
      />
    );
  }
  return <span>{values[props.value] || ''}</span>;
};

RejectReason.propTypes = {
  onChange: PropTypes.func,
  row: PropTypes.shape({
    reject: PropTypes.shape({
      reason: PropTypes.string,
    }),
  }),
};

RejectReason.defaultProps = {
  onChange: () => false,
  row: { reject: { reason: '' } },
};

export default RejectReason;
