import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './Columns.sass';

class Columns extends Component {

  renderChild(child) {
    const { columnsCount } = this.props;

    if (child.props.indicator === 'ColumnsRow') {
      return React.cloneElement(
        child,
        {
          children: child.props.children,
          columnsCount: child.props.columnsCount || columnsCount,
        },
      );
    }

    return child;
  }

  render() {
    const { className, children } = this.props;
    return (
      <div className={`columns ${className}`}>
        {React.Children.map(children, child => this.renderChild(child))}
      </div>
    );
  }
}

Columns.propTypes = {
  className: PropTypes.string,
  columnsCount: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element,
  ]).isRequired,
};

Columns.defaultProps = {
  className: '',
  columnsCount: null,
};

// Пример использования:
/* <Columns>
    <ColumnsRow>

     <ColumnsGroup title="Акт списания">
      <Column>
        <Input label="Номер документа" name="reject_doc_num" form />
      </Column>
       <Column>
        <SingleDatePicker label="Дата выдачи документа" name="reject_doc_date" form />
       </Column>
     </ColumnsGroup>

     <ColumnsGroup title="Акт списания">
      <Column>
        <Input label="Номер документа" name="reject_doc_num" form />
      </Column>
     </ColumnsGroup>

     <Column>
        <Text>Hello</Text>
      </Column>
    </ColumnsRow>
 </Columns> */

export default Columns;
