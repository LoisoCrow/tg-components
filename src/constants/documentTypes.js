const documentTypes = [
  {
    action_id: 0,
    name: 'Отзыв заявки на изготовление КиЗ',
    element_xml: 'recall_waiting',
  },
  {
    action_id: 4,
    name: 'Модерация контролирующим органом',
    element_xml: 'moderate_system',
  },
  {
    action_id: 10,
    name: 'Заявка на изготовление КиЗ',
    element_xml: 'emit_order',
  },
  {
    action_id: 11,
    name: 'Изменение статуса заявки  на эмиссию КиЗ',
    element_xml: 'emit_order_status',
  },
  {
    action_id: 12,
    name: 'Сведения о эмитированных и поставленных КиЗ эмитентом',
    element_xml: 'emit_signs',
  },
  {
    action_id: 13,
    name: 'Прием (отклонение) КиЗ',
    element_xml: 'commit_signs',
  },
  {
    action_id: 14,
    name: 'Закрытие субъектом заявки на эмиссию КиЗ',
    element_xml: 'commit_order',
  },
  {
    action_id: 15,
    name: 'Выбраковка неиндивидуализированных КиЗ',
    element_xml: 'ununify_reject',
  },
  {
    action_id: 17,
    name: 'Подача дополнительных сведений по КиЗ',
    element_xml: 'sign_detail',
  },
  {
    action_id: 18,
    name: 'Сведения о перемаркировке товара',
    element_xml: 'sign_detail_remerked',
  },
  {
    action_id: 20,
    name: 'Сведения о маркировке',
    element_xml: 'unify_self_signs',
  },
  {
    action_id: 21,
    name: 'Подача субъектом сведений об индивидуализации и маркировании общих КиЗ',
    element_xml: 'unify_common_signs',
  },
  {
    action_id: 22,
    name: 'Сведения о списании (выбраковке) КиЗ',
    element_xml: 'reject_signs',
  },
  {
    action_id: 23,
    name: 'Сведения о возврате товара покупателем',
    element_xml: 'return_signs',
  },
  {
    action_id: 28,
    name: 'Прием (отклонение) товара по одной накладной',
    element_xml: 'move_order_commit_signs',
  },
  {
    action_id: 29,
    name: 'Прием (отклонение) товара из разных накладных',
    element_xml: 'm_commit_signs',
  },
  {
    action_id: 30,
    name: 'Заявка отправителя на отгрузку маркированной продукции',
    element_xml: 'move_order',
  },
  {
    action_id: 31,
    name: 'Отмена получателем заявки на отгрузку маркированной продукции',
    element_xml: 'move_order_cancel',
  },
  {
    action_id: 32,
    name: 'Подтверждение получателем поставленной маркированной продукции',
    element_xml: 'move_commit_signs',
  },
  {
    action_id: 33,
    name: 'Закрытие получателем заявки отправителя на отгрузку',
    element_xml: 'move_order_commit',
  },
  {
    action_id: 34,
    name: 'Заявка отправителя на отгрузку маркированной продукции незарегистрированному участнику',
    element_xml: 'undefined_order',
  },
  {
    action_id: 38,
    name: 'Отмена отправителем заявки на отгрузку маркированной продукции по одной накладной',
    element_xml: 'move_order_cancel_sender',
  },
  {
    action_id: 39,
    name: 'Отмена отправителем заявки на отгрузку маркированной продукции из разных накладных',
    element_xml: 'm_cancel_sender',
  },
  {
    action_id: 40,
    name: 'Сведения о розничной продаже маркированной продукции',
    element_xml: 'retail_sale',
  },
  {
    action_id: 41,
    name: 'Сведения о трансграничной поставке маркированной продукции в страну ЕАЭС',
    element_xml: 'export_sale',
  },
  {
    action_id: 42,
    name: 'Сведения при временном вывозе и последующем ввозе',
    element_xml: 'temp_export',
  },
  {
    action_id: 43,
    name: 'Вывод из оборота в связи со списанием товара, возвратом физ. лицу или экспортом не в страны ЕАЭС',
    element_xml: 'item_removal',
  },
  {
    action_id: 50,
    name: 'Сведения о импорте маркированной продукции из стран ЕАЭС',
    element_xml: 'import_signs',
  },
];

export default documentTypes;
