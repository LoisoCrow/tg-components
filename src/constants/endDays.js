import map from 'lodash/map';

const endDays = {
  '-1': 'Истек',
  '0': 'Истекает сегодня',
};

export const filterOptions = map(endDays, (name, value) => ({
  value: +value,
  name,
}));

export default endDays;
