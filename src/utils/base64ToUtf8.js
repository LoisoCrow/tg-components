import utf8 from 'utf8';
import base64 from 'base-64';

export default (encoded) => {
  const bytes = base64.decode(encoded);
  return utf8.decode(bytes);
};
