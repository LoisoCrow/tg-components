import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { PropTypes } from 'prop-types';
import { routeMap } from 'router/routes';
import './Sidebar.sass';

const pages = {
  main: {
    title: 'Главная',
    link: routeMap.home(),
  },
  kiz: {
    title: 'Работа с КиЗ',
    link: routeMap.kiz(),
  },
  products: {
    title: 'Товары',
    link: routeMap.products(),
  },
  transport: {
    title: 'Трансграничная торговля',
    link: routeMap.transport(),
  },
  settings: {
    title: 'Настройки',
    link: routeMap.settings(),
  },
  actions: {
    title: 'Действия',
    link: routeMap.actions(),
  },
};

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: props.isExpanded,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isExpanded !== this.state.isExpanded) {
      this.setState({ isExpanded: nextProps.isExpanded });
    }
  }

  isActive = (match) => {
    if (!match) {
      return false;
    }

    if (match.path === '/' && !match.isExact) {
      return false;
    }

    return true;
  }

  toggleMenu = () => {
    this.setState({ isExpanded: !this.state.isExpanded });
  };

  handleLogoutUser = () => {
    this.props.logout();
  };

  renderPageLinks = () => Object.keys(pages).map((pageName) => {
    const { link, title } = pages[pageName];
    return (
      <li key={pageName}>
        <NavLink to={link} isActive={this.isActive} activeClassName="-active">
          <i className={`sidebar__icon sidebar__icon__${pageName}`} />
          {title}
        </NavLink>
      </li>
    );
  })

  renderMenuButton = () => {
    const { isExpanded } = this.state;
    const menuClassNames = classnames('menu', { 'menu-close': isExpanded });
    return (
      <li onClick={this.toggleMenu}>
        <div className="sidebar__navigation__menu">
          <div className={menuClassNames}>
            <div className="menu__line menu__line-first" />
            <div className="menu__line menu__line-second" />
            <div className="menu__line menu__line-third" />
          </div>
        </div>
      </li>
    );
  }

  renderOrganizationName = organizationName => (
    <li className="sidebar__navigation__organizationName">
      <a className="-not-active">
        {organizationName}
      </a>
    </li>
  )

  renderUser = (name, position) => (
    <li className="sidebar__navigation__profile">
      <a className="-not-active">
        <i className="sidebar__icon sidebar__icon__person" />
        <div className="sidebar__navigation__profile-user">
          <div>
            {name}<br />
            <span className="sidebar__navigation__profile-user-position">{position}</span>
          </div>
          <i
            role="presentation"
            onClick={this.handleLogoutUser}
            className="sidebar__icon sidebar__icon__logout"
          />
        </div>
      </a>
    </li>
  )

  renderFooter = () => (
    <div className="sidebar__footer">
      <nav className="sidebar__navigation">
        <li className="sidebar__navigation__copyright">
        Текст содержащий копирайт об основном сервисе,
        с указанием ссылок на источники, правообладателей
        </li>
        <li className="sidebar__navigation__reference">
          <NavLink to={routeMap.help()} exact isActive={this.isActive} activeClassName="-active">
            <i className="sidebar__icon sidebar__icon__ref" />
                Справка
          </NavLink>
        </li>
      </nav>
    </div>
  )

  render() {
    const {
      isAuthorized,
      organizationName,
      user: {
        name,
        position,
      },
    } = this.props;

    const { isExpanded } = this.state;

    const classNames = classnames('sidebar', {
      sidebar__expanded: isExpanded,
    });

    return (
      <div className={classNames}>
        <nav className="sidebar__navigation">
          {this.renderMenuButton()}
          {isAuthorized && this.renderOrganizationName(organizationName)}
          {isAuthorized && this.renderUser(name, position)}
          {isAuthorized && this.renderPageLinks()}
        </nav>
        {this.renderFooter()}
      </div>
    );
  }
}

Sidebar.propTypes = {
  isAuthorized: PropTypes.bool,
  organizationName: PropTypes.string,
  isExpanded: PropTypes.bool,
  user: PropTypes.shape({
    name: PropTypes.string,
    position: PropTypes.string,
  }),
};

Sidebar.defaultProps = {
  isAuthorized: false,
  organizationName: null,
  isExpanded: false,
  user: {
    name: '',
    position: '',
  },
};

export default Sidebar;
