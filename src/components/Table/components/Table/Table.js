import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import classnames from 'classnames';
import { isEmpty } from 'lodash';
import { Scrollbars } from 'react-custom-scrollbars';
import Row from '../Row/Row';
import './Table.sass';
import ToggleColumn from '../ToggleColumn/ToggleColumn';
import Sorting from '../Sorting/Sorting';

const CHEKBOXCOLUMN_WIDTH = 57;
const TOGGLECOLUMN_WIDTH = 151;

class Table extends PureComponent {
  constructor(props) {
    super(props);
    const { columns } = props;
    const width = this.countTableWidth(columns);
    if (!this.checkProtectedColumn(columns)) {
      console.warn('Хотя бы одно поле таблицы должно иметь свойство protected');// eslint-disable-line
    }
    this.state = {
      width,
      activeColumn: '',
    };
  }

  componentDidMount() {
    const { savedScroll } = this.props;
    if (savedScroll) {
      this.scrollbar.scrollLeft(savedScroll);
    }
  }

  componentWillReceiveProps(nextProps) {
    this.updateTableWidth(nextProps.columns);
  }

  componentWillUnmount() {
    const { saveScroll } = this.props;
    if (saveScroll) {
      saveScroll(this.scrollbar.getScrollLeft());
    }
  }

  checkProtectedColumn = columns => columns.some(column => column.protected);

  updateTableWidth = (columns) => {
    const width = this.countTableWidth(columns);
    if (width !== this.state.width) {
      this.setState({ width });
    }
  };

  countTableWidth = (columns) => {
    const { options } = this.props;
    const iterator = (accumulator, column) => {
      if (column.hidden) {
        return accumulator;
      }
      return column.width + accumulator;
    };
    return columns.reduce(iterator, 0)
      + (options.selectColumn ? CHEKBOXCOLUMN_WIDTH : 0)
      + (options.toggleColumns ? TOGGLECOLUMN_WIDTH : 0);
  }

  sortHandler = (sort) => {
    const { setSorting: onSort } = this.props;
    this.setState({
      activeColumn: sort[0].nameAttribute,
    });
    onSort(sort);
  };

  // TODO: move to reducer
  // deleteRowHandler = (id) => {
  //   const {
  //     onRowDelete,
  //     data,
  //   } = this.props;

  //   const newData = clone(data);

  //   const index = newData.map(row => row._id).indexOf(id);
  //   newData.splice(index, 1);

  //   onRowDelete(newData);
  // };

  formRows = () => {
    const {
      data,
      options,
      disable,
      columns,
      rowActions,
    } = this.props;
    let selectProps = {};
    if (options.selectColumn) {
      const {
        selectMember: onSelect,
        selected,
      } = this.props;
      selectProps = {
        multiselect: options.multiselect,
        onSelect,
        selected,
      };
    }
    const columnLength = columns.length
      + (options.selectColumn ? 1 : 0)
      + (options.toggleColumns ? 1 : 0);
    return isEmpty(data) ? (
      <tr className="tr-empty">
        <td
          colSpan={columnLength}
          className="text-center"
        >
          <div className="td-inner">
          Нет данных
          </div>
        </td>
      </tr>
    ) : (
      data.map((row) => {
        const disabled = row.last_doc ?
          (row.last_doc.status === 0 || row.last_doc.status === 2) :
          false;
        return (
          <Row
            toggleActions={options.toggleActions}
            disabled={!('disabledRows' in options) || options.disabledRows ? disabled || disable(row) : false}
            key={row._id}
            data={row}
            columns={columns}
            toggleColumns={options.toggleColumns}
            selectColumn={options.selectColumn}
            checkboxName={`checkbox${row._id}`}
            {...selectProps}
            rowActions={rowActions}
          />
        );
      })
    );
  };

  formHeadersData = columns => columns.filter(f => !f.hidden).map((column, index, columnArray) => {
    const { field, header, sorting, sortingName, hidden } = column;
    const Child = sorting
      ? (<Sorting
        key={header}
        sortHandler={this.sortHandler}
        sort={this.props.sort}
        title={header}
        sortingName={sortingName || column.field}
      />)
      : <div>{header}</div>;
    const active = field === this.state.activeColumn;
    const className = classnames(
      'table__th',
      { hide: hidden, __active: active, 'table__th-last': columnArray.length - 1 === index },
    );
    return (
      <th
        key={column.field}
        className={className}
      >
        <div className="th-inner">
          {Child}
        </div>
      </th>
    );
  });

  renderCheckboxHeader = () => (
    <th
      key="selected"
      className="table__checkbox-th"
    >
      <div className="th-inner" />
    </th>
  );

  renderEditHeader = () => {
    const { columns, setColumns } = this.props;
    return (
      <th key="edit" className={'table__editable-th'}>
        <div className="th-inner">
          <ToggleColumn toggleColumns={setColumns} columns={columns} />
        </div>
      </th>
    );
  };

  renderHeader = () => {
    const { options } = this.props;
    const checkboxHeader = options.selectColumn ?
      this.renderCheckboxHeader() :
      this.renderEmptyFirstColumn();
    const dataHeaders = this.formHeadersData(this.props.columns);
    const editHeader = options.toggleColumns ? this.renderEditHeader() : null;
    return (
      <tr>
        {[checkboxHeader, ...dataHeaders, editHeader]}
      </tr>
    );
  };

  renderEmptyFirstColumn = () => (
    <th key="empty-th" className="table__th table__th-empty">
      <div className="th-inner" />
    </th>
    )


  renderView = ({ style, ...props }) => (
    <div
      className="table__container-body"
      style={{ ...style, position: 'static' }}
      {...props}
    />
  )

  renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
      backgroundColor: '#0096ff',
      position: 'fixed',
      bottom: '65px',
      height: '5px',
    };
    return (
      <div
        className="table__container-scroll"
        style={{ ...style, ...thumbStyle }}
        {...props}
      />
    );
  }

  render() {
    const { options } = this.props;
    const tableContentClassName = classnames('table__content');
    const tableContainer = classnames(
      'table__container',
      { 'table__container-withcheckboxcolumn': options.selectColumn },
      { 'table__container-withtogglecolumn': options.toggleColumns });
    return (
      <div className={tableContainer}>
        <div className={tableContentClassName}>
          <Scrollbars
            ref={(scrollbar) => { this.scrollbar = scrollbar; }}
            autoHeight
            autoHeightMax={5000}
            autoHeightMin={'100%'}
            renderView={this.renderView}
            renderThumbHorizontal={this.renderThumb}
          >
            <table className="paper_table" style={{ width: this.state.width }}>
              <thead>
                {this.renderHeader()}
              </thead>
              <tbody>
                {this.formRows()}
              </tbody>
            </table>
          </Scrollbars>
        </div>
      </div>
    );
  }
}

Table.propTypes = {
  data: PropTypes.array.isRequired,
  selected: PropTypes.array,
  sort: PropTypes.array,
  columns: PropTypes.arrayOf(PropTypes.shape({
    header: PropTypes.string.isRequired,
    field: PropTypes.string.isRequired,
    hidden: PropTypes.bool,
    sorting: PropTypes.bool,
    width: PropTypes.number.isRequired,
    flexible: PropTypes.bool,
    protected: PropTypes.bool,
    component: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.func,
    ]),
    componentProps: PropTypes.object,
    sortingName: PropTypes.string,
    dependent: PropTypes.bool,
  })).isRequired,
  options: PropTypes.shape({
    multiselect: PropTypes.bool,
    toggleColumns: PropTypes.bool,
    selectColumn: PropTypes.bool,
    toggleActions: PropTypes.shape({
      action: PropTypes.func,
    }),
  }),
  selectMember: PropTypes.func,
  setSorting: PropTypes.func,
  setColumns: PropTypes.func,
  disable: PropTypes.func,
  rowActions: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.string,
      label: PropTypes.string,
      event: PropTypes.func,
      condition: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.func,
      ]),
    }),
  ),
};

Table.defaultProps = {
  selected: [],
  sort: [],
  options: {
    toggleColumns: true,
    selectColumn: true,
    disabledRows: true,
  },
  loading: false,
  saveScroll: () => 0,
  disable: () => false,
  selectMember: () => true,
  setSorting: () => true,
  setColumns: () => [],
  rowActions: [],
};

export default Table;
