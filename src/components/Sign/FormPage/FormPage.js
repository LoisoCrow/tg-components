import React, { Component } from 'react';
import { Form } from 'react-final-form';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { moment, toDefaultFormat } from 'utils';
import { Content, Alert, Footer, Button, Loader, SubHeader } from 'components';
import { SignModal, SignSuccessModal } from '../';
import './FormPage.sass';

const ERROR_GENERATING_XML = 'Ошибка формирования XML';
const ERROR_SIGNING_XML = 'Ошибка подписания XML';
const ERROR_XML = 'Проверьте правильность заполненных данных в форме';

class FormPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      signModal: false,
      successModal: false,
    };
    this.nowDate = moment();
  }

  componentWillReceiveProps(nextProps) {
    const {
      generateXmlAttributes: { success: currentXmlSuccess },
      signXmlAttributes: { success: currentSignSuccess },
      onFinish,
    } = this.props;
    const {
      generateXmlAttributes: { error: newXmlError, success: newXmlSuccess },
      signXmlAttributes: { error: newSignError, success: newSignSuccess },
      base64Xml,
      finished,
    } = nextProps;
    if (newXmlError) {
      this.toggleError(ERROR_GENERATING_XML);
    } else if (newSignError) {
      this.toggleError(ERROR_SIGNING_XML);
    }

    if (!currentXmlSuccess && newXmlSuccess && base64Xml) {
      this.setState({ signModal: true });
    }

    if (!currentSignSuccess && newSignSuccess) {
      this.setState({ successModal: true });
    }
    if (onFinish && finished) {
      onFinish();
    }
  }

  handleSubmit = async (values) => {
    const { prepareData, generateXml, validateData } = this.props;
    const validation = validateData(values);
    if (validation === true) {
      const xmlData = prepareData({ ...values, statementDate: this.nowDate.format('YYYY-MM-DD[T]HH:mm:ss[Z]') });
      this.setState({ xmlData, error: null });
      try {
        await generateXml(xmlData);
      } catch (e) {
        this.toggleError(ERROR_GENERATING_XML);
      }
    } else {
      this.setState({ error: validation || ERROR_XML });
    }
  }

  handleFieldChanges = () => {
    const { error } = this.state;

    if (error) {
      this.toggleError();
    }
  }

  signXml = async () => {
    const { signXml, base64Xml, certificate } = this.props;
    this.setState({ signModal: false });
    try {
      await signXml(base64Xml, certificate, this.values);
    } catch (e) {
      this.toggleError(ERROR_SIGNING_XML);
    }
  }

  toggleError = (error = null) => {
    this.setState({ error });
  }

  renderButtons = () => {
    const { submitBtnLabel, draftBtnLabel, noDraft } = this.props;

    return (
      <div className="formPage__footer__actions">
        {
          !noDraft && (
            <Button
              onPress={() => this.values}
              disabled
            >
              {draftBtnLabel}
            </Button>
          )
        }
        <Button
          onPress={this.handleSubmit}
        >
          {submitBtnLabel}
        </Button>
      </div>
    );
  }

  renderLoader = () => {
    const {
      generateXmlAttributes: { loading: generating },
      signXmlAttributes: { loading: signing },
    } = this.props;

    return (<Loader
      withContainer
      visible={generating || signing}
    />);
  }

  renderSignModal = () => {
    const { signModal, xmlData } = this.state;
    const { xml, title, generateStatementDescription } = this.props;

    if (signModal) {
      return (
        <SignModal
          show={signModal}
          signAndSend={this.signXml}
          xml={xml}
          title={`${title} от ${toDefaultFormat(this.nowDate)}`}
          closeModal={() => { this.setState({ signModal: false }); }}
          statementDescription={generateStatementDescription(xmlData)}
        />
      );
    }

    return null;
  }

  renderSuccessModal = () => {
    const { successModal } = this.state;
    const { onSuccessSign, title } = this.props;

    if (successModal) {
      return (
        <SignSuccessModal
          show
          title={title}
          closeModal={() => { onSuccessSign(); this.setState({ successModal: false }); }}
        />
      );
    }

    return null;
  }

  renderHeader = () => {
    const { title, handleCloseClick, subHeaderStatus } = this.props;

    return (
      <SubHeader
        title={`${title}`}
        subTitle={`от ${this.nowDate.format('DD.MM.YYYY')}`}
        handleBackClick={handleCloseClick}
      >
        <span>Уважаемый пользователь!<br /> Для регистрации в качестве участника оборота
          товаров в информационном ресурсе маркировки товаров
          из натурального меха заполните реквизиты:</span>
        {subHeaderStatus && <h2 className="bold">{subHeaderStatus}</h2>}
      </SubHeader>
    );
  }

  renderError = (customError = null) => {
    const { error } = this.state;
    const { table } = this.props;

    const className = classnames('formPage__error', { 'formPage__error-fixed': !table });

    if (error || customError) {
      return (<div className={className}>
        <Alert error text={customError || error} />
      </div>);
    }

    return null;
  }

  renderFooter = () => <Footer className="formPage__footer" right={() => this.renderButtons()} />

  render() {
    const { children, table } = this.props;

    return (
      <div className="formPage">
        {this.renderHeader()}
        <Form
          onSubmit={this.handleSubmit}
          validate={this.handleFieldChanges}
          render={({ handleSubmit, submitFailed, submitting, values }) => {
            this.handleSubmit = handleSubmit;
            this.values = values;
            return (
              <div className="formPage__main">
                <Content className="formPage__content">
                  <div className="formPage__fields">
                    {children}
                  </div>
                </Content>
                {this.renderError(submitFailed && !submitting ? ERROR_XML : null)}
              </div>
            );
          }}
        />
        <div className="formPage__table">
          {table && table(this.handleSubmit, this.values)}
        </div>
        {!table && this.renderFooter()}
        {this.renderLoader()}
        {this.renderSignModal()}
        {this.renderSuccessModal()}
      </div>
    );
  }
}

FormPage.propTypes = {
  title: PropTypes.string.isRequired,
  subHeaderStatus: PropTypes.string,
  handleCloseClick: PropTypes.func.isRequired,
  submitBtnLabel: PropTypes.string,
  draftBtnLabel: PropTypes.string,
  prepareData: PropTypes.func.isRequired,
  xml: PropTypes.string,
  generateXmlAttributes: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.string,
    success: PropTypes.bool,
  }).isRequired,
  signXmlAttributes: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.string,
    success: PropTypes.bool,
  }).isRequired,
  base64Xml: PropTypes.string,
  generateStatementDescription: PropTypes.func,
  table: PropTypes.func,
  validateData: PropTypes.func,
  onSuccessSign: PropTypes.func,
};

FormPage.defaultProps = {
  subHeaderStatus: '',
  submitBtnLabel: 'Подписать и отправить',
  draftBtnLabel: 'Сохранить черновик',
  prepareData: values => values,
  generateStatementDescription: () => null,
  xml: '',
  base64Xml: '',
  table: null,
  validateData: () => true,
  onSuccessSign: () => {},
};

export default FormPage;
