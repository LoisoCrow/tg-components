export const NOT_REMARKING = 0;
export const REMARKING = 1;

export default {
  [NOT_REMARKING]: 'не является КиЗ, участвующим в перемаркировке',
  [REMARKING]: 'КиЗ участвует в перемаркировке',
};
