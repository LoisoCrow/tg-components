import PropTypes from 'prop-types';

const RemarkedType = ({ value }) => {
  if (value && +value === 1) {
    return 'Да';
  }
  return 'Нет';
};

RemarkedType.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

export default RemarkedType;
