import config from 'config';
import { API } from 'services';

export const fetch = async (data, endpoint, action_id, v) => {
  const fetchConfig = {
    url: endpoint,
    method: 'POST',
    data: {
      ...data,
    },
  };

  if (action_id) {
    data.action_id = action_id;
  }

  return API.request(fetchConfig, v);
};

export const send = async (data, endpoint, code, v) => {
  const sendConfig = {
    url: `documents/send/${endpoint}/${code}`,
    method: 'POST',
    data,
  };
  const version = config.cryptographyEnabled ? 'v1' : 'v0';

  return API.request(sendConfig, v || version);
};
