import React, { Component } from 'react';
import classNames from 'classnames';
import { PropTypes } from 'prop-types';
import './Alert.sass';

class Alert extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: props.collapsed,
    };
  }

  toggleCollapsed = () => {
    const { collapsible } = this.props;
    if (collapsible) {
      this.setState({ collapsed: !this.state.collapsed });
    }
  }

  className = error => classNames(
    'alert',
    { alert__collapsed: this.state.collapsed },
    { alert__error: error },
    { alert__info: !error },
  );

  contentClassName = () => classNames(
    'alert__content',
  )

  icoClassName = collapsed => classNames(
    'icon',
    { 'icon__arrow-down': collapsed },
    { 'icon__arrow-up': !collapsed },
  )

  isVisible() {
    const { collapsed } = this.state;
    const { collapsible } = this.props;
    return !collapsed || !collapsible;
  }

  renderContent = () => {
    const {
      text,
      children,
      collapsible,
    } = this.props;
    const { collapsed } = this.state;
    const isVisible = this.isVisible();
    return (
      <div className={this.contentClassName(collapsed)}>
        <div className="alert__content__text">
          {isVisible && text}
          {isVisible && children}
        </div>
        {collapsible && <i
          role="presentation"
          onClick={this.toggleCollapsed}
          className={this.icoClassName(collapsed)}
        />}
      </div>
    );
  }

  render() {
    const { error } = this.props;
    return (
      <div className={this.className(error)}>
        {this.renderContent()}
      </div>
    );
  }
}

Alert.propTypes = {
  collapsed: PropTypes.bool,
  collapsible: PropTypes.bool,
  text: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
    PropTypes.arrayOf(
      PropTypes.element,
    ),
  ]),
  error: PropTypes.bool,
};

Alert.defaultProps = {
  collapsed: false,
  collapsible: true,
  text: null,
  children: null,
  error: false,
};

export default Alert;
