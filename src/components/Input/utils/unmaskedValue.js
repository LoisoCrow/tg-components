const getUnmaskedValue = (maskedValue, mask) => {
  const maskAsArr = mask.split('');
  const valueToUnmask = maskedValue.split('');
  for (let i = 0; i < maskAsArr.length; i += 1) {
    const found = valueToUnmask.indexOf(maskAsArr[i]);
    if (found >= 0) {
      valueToUnmask.splice(found, 1);
    }
  }
  return valueToUnmask.join('');
};

export default getUnmaskedValue;
