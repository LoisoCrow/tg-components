import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'components';

class TextFilter extends Component {
  onChange = (value) => {
    this.props.onChange({ text: value });
  }

  render() {
    return (
      <Input
        {...this.props}
        onChange={this.onChange}
        value={this.props.value.text}
      />
    );
  }
}

TextFilter.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

TextFilter.defaultProps = {
  filter: true,
  value: { text: '' },
};

export default TextFilter;
