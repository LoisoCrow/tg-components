import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './StatisticsBlock.sass';


class StatisticsBlock extends Component {
  getItem = (item, ind) => {
    const { label, value } = item;
    return (
      <div className="statistics-block__item" key={ind}>
        <div className="label">{label}</div>
        <div className="value">{value}</div>
      </div>
    );
  };

  calcWidth = (val, width) => `${(val * 100) / width}%`;

  renderGraph = (graphs) => {
    const { red, green } = graphs;
    const width = (red.value * 1) + (green.value * 1);

    return (
      <div className="statistics-block__graph">
        <div className="statistics-block__graph__item">
          <span className="__caption">{green.label}</span>
          <div className="statistics-block__graph__green">
            <div
              className="__line"
              style={{ width: this.calcWidth((green.value * 1), width) }}
            />
            <span>{green.value}</span>
          </div>
        </div>

        <div className="statistics-block__graph__item">
          <span className="__caption">{red.label}</span>
          <div className="statistics-block__graph__red">
            <div
              className="__line"
              style={{ width: this.calcWidth((red.value * 1), width) }}
            />
            <span>{red.value}</span>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { data, graph: { red, green } } = this.props;

    return (
      <div className="statistics-block">
        <div className="statistics-block__items">
          {data.map((i, ind) => this.getItem(i, ind))}
        </div>
        {Object.keys(this.props.graph).length > 0 && this.renderGraph({ red, green })}
      </div>
    );
  }

}

StatisticsBlock.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]).isRequired,
  })),
  graph: PropTypes.shape({
    red: PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]).isRequired,
    }),
    green: PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]).isRequired,
    }),
  }),
};

StatisticsBlock.defaultProps = {
  data: [],
  graph: {},
};

export default StatisticsBlock;
