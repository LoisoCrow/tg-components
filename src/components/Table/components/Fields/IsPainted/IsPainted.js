import React from 'react';

const IsPainted = ({ value }) => <span>{value ? 'Да' : 'Нет'}</span>;

export default IsPainted;
