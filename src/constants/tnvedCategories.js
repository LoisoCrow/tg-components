import map from 'lodash/map';

const tnvedCategories = {
  4303109010: 'Предметы одежды из норки',
  4303109020: 'Предметы одежды из нутрии',
  4303109030: 'Предметы одежды из песца или лисицы',
  4303109040: 'Предметы одежды из кролика или зайца',
  4303109050: 'Предметы одежды из енота',
  4303109060: 'Предметы одежды из овчины',
  4303109080: 'Предметы одежды прочие',
};

export const filterOptions = map(tnvedCategories, (name, value) => ({ name: `${value} - ${name}`, value }));

export default tnvedCategories;
