import moment from 'moment';
import { toDefaultFormat, toServerFormat } from './moment';


test('sets locale to ru', () => {
  expect(moment.locale()).toBe('ru');
});

test('converts dateString to default format', () => {
  expect(toDefaultFormat('1989-01-12')).toBe('12.01.1989');
});

test('converts dateString to server format', () => {
  expect(toServerFormat('12.01.1989')).toBe('1989-01-12');
});

test('converts invalid date to an empty string', () => {
  expect(toServerFormat('')).toBe('');
  expect(toDefaultFormat('')).toBe('');
});
