const createReducer = (initialState, map) => (state = initialState, action) => {
  if (Object.hasOwnProperty.call(map, action.type)) {
    return map[action.type](state, action);
  }
  return state;
};

export default createReducer;
