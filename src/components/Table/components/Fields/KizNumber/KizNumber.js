import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { isGreen } from 'utils/rules';
import { RED, GREEN, colors } from 'constants/colors';
import './KizNumber.sass';

const KizNumber = ({ row }) => {
  // const { number_kiz, origin } = row;
  const { color, number_kiz, kind_kiz } = row;
  let col = '';

  // if (!origin) {
  //   return number_kiz;
  // }
  if (!color) {
    if (kind_kiz) {
      col = isGreen(kind_kiz) ? colors[GREEN] : colors[RED];
    } else {
      return number_kiz;
    }
  } else {
    col = color;
  }

  const className = classNames('kiz__circle', {
    'kiz__circle-green': col === colors[GREEN],
    'kiz__circle-red': col === colors[RED],
  });

  return (
    <div className="kiz">
      <i className={className} />
      <span>{number_kiz}</span>
    </div>
  );
};

KizNumber.propTypes = {
  row: PropTypes.shape({
    color: PropTypes.string.isRequired,
    number_kiz: PropTypes.string.isRequired,
  }).isRequired,
};

export default KizNumber;
