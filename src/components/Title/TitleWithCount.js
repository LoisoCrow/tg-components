import React from 'react';
import { formatNumber } from 'utils';
import Title from './Title';
import './Title.sass';

const TitleWithCount = ({ children, count, label, ...otherProps }) => (
  <div className="page__title__count">
    <Title {...otherProps}>{children}</Title>
    {count > 0 ? <div className="page__title__count page__title__count__label">{label}:
      <span className="page__title__count__number">{formatNumber(count)}</span>
    </div> : null }
  </div>
);

export default TitleWithCount;
