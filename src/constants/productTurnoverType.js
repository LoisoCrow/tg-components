import { map } from 'lodash';

const productTurnoverType = {
  1: 'Продажа',
  2: 'Договор комиссии',
  3: 'Агентский договор',
  4: 'Прочее',
  5: 'Безвозмездная передача',
  6: 'Передача в связи с банкротством',
  7: 'Передача в связи с ликвидацией',
  8: 'Передача конфискованного товара',
};

export const filterOptions = map(productTurnoverType, (name, value) => ({
  value: Number(value),
  name,
}));

export default productTurnoverType;
