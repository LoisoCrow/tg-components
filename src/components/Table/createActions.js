/**
 * @typedef {Object} Actions
 * @property {function} setPagination
 * @property {function} setFilter
 * @property {function} setSorting
 * @property {function} selectMember
 * @property {function} getData
 *
 * @typedef {Object} thunk
 * @property {string} request - redux action type
 * @property {string} success - redux action type
 * @property {string} error - redux action type
 *
 * @typedef {Object} handlers
 * @property {string} select - redux action type
 * @property {string} sort - redux action type
 * @property {string} page - redux action type
 * @property {string} filter - redux action type
 * @property {string} delete - redux action type
 * @property {string} columns - redux action type
 *
 * @typedef {Object} constants
 * @property {thunk}
 * @property {handlers}
 *
 * @param {constants} constants - redux action types structure
 * @param {function} apiRequest - returns http response
 *
 * @returns {Object} actions
 * }
*/
const createActions = (constants, apiRequest = () => {}) => {
  let { thunk, handlers } = constants;
  if (!thunk && !handlers) {
    thunk = constants.types.thunk;
    handlers = constants.types.handlers;
  }

  const actions = {};

  if (handlers) {
    if (handlers.page) {
      actions.setPagination = pagination => ({
        type: handlers.page,
        payload: pagination,
      });
    }

    if (handlers.select) {
      actions.selectMember = selected => ({
        type: handlers.select,
        payload: selected,
      });
    }

    if (handlers.sort) {
      actions.setSorting = sortObject => ({
        type: handlers.sort,
        payload: sortObject,
      });
    }

    if (handlers.filter) {
      actions.setFilter = filter => ({
        type: handlers.filter,
        payload: filter,
      });
    }

    if (handlers.delete) {
      actions.onRowDelete = id => ({
        type: handlers.delete,
        payload: id,
      });
    }

    if (handlers.columns) {
      actions.setColumns = columns => ({
        type: handlers.columns,
        payload: columns,
      });
    }

    if (handlers.saveScroll) {
      actions.saveScroll = scroll => ({
        type: handlers.scroll,
        payload: scroll,
      });
    }
  }

  if (thunk && apiRequest) {
    const request = () => ({
      type: thunk.request,
    });

    const success = data => ({
      type: thunk.success,
      payload: data,
    });

    const error = message => ({
      type: thunk.error,
      payload: message,
    });

    actions.getData = query => async (dispatch) => {
      dispatch(request());
      try {
        const response = await apiRequest(query);
        const { data } = response;

        if (data) {
          dispatch(success(data));
        } else {
          dispatch(error('Empty response'));
        }
      } catch (exception) {
        dispatch(error(exception));
      }
    };
  }

  return actions;
};

export default createActions;
