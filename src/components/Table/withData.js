import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/**
 *
 * @param {Component} WrappedComponent - react component
 * @param {string} statePath - path to table state (ex. 'shipment.allShipped')
 * @param {object} actionCreators - created with createActions func
 *
 * @returns {Component}
 * }
 * */
export const withData = (WrappedComponent, statePath, actionCreators) => class extends Component {
  constructor(props) {
    super(props);

    this.ConnectedComponent = connect(
      this.mapStateToProps,
      this.mapDispatchToProps,
    )(WrappedComponent);
  }

  mapStateToProps = (state) => {
    const nameArray = statePath.split('.');
    let scope = state;
    while (nameArray.length) {
      if (!scope) {
        scope = null;
        break;
      }
      scope = scope[nameArray.shift()];
    }
    return scope;
  }

  mapDispatchToProps = dispatch => bindActionCreators(actionCreators, dispatch);

  render() {
    return (
      <this.ConnectedComponent {...this.props} />
    );
  }
};
