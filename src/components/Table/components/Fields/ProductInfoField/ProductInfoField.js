import React from 'react';
import PropTypes from 'prop-types';
import './ProductInfoField.sass';

const ProdInfoField = ({ value, row, openModal }) => (
  <div className="prod-info-field">
    {row.gtin && <div onClick={() => openModal(row.gtin)} className="button">
      <div className="icon" />
    </div>}
    <span>{value}</span>
  </div>
);

ProdInfoField.propTypes = {
  value: PropTypes.string.isRequired,
  row: PropTypes.shape({
    gs1Info: PropTypes.object,
  }).isRequired,
  openModal: PropTypes.func,
};

ProdInfoField.defaultProps = {
  openModal: () => false,
};

export default ProdInfoField;
