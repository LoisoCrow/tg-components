import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import classNames from 'classnames';
import { Tooltip } from 'components';

class Cell extends Component {
  className = () => classNames(
    { 'td-disabled': this.props.disabled },
  );

  renderComponent = () => {
    const {
      value,
      componentProps,
      row,
      disabled,
      header,
    } = this.props;
    const Child = this.props.component;
    return (
      <Child
        {...componentProps}
        header={header}
        disabled={disabled}
        row={row}
        onChange={!disabled ? componentProps.onChange : null}
        value={value}
      />
    );
  };

  render() {
    const { component, value, width, flexible, className, componentProps, tooltip } = this.props;
    const editableCls = componentProps.editable ? 'editable' : '';

    const child = component ? this.renderComponent() : value;

    return (
      <td
        className={`${`${className}`} ${this.className()} ${editableCls}`}
        style={width && !flexible
          ? { width, maxWidth: width, minWidth: width }
        : { minWidth: width }}
      >
        <div className="td-inner">
          {tooltip
            ? <Tooltip callElement={child} position="top"><div>{tooltip}</div></Tooltip>
            : child
          }
        </div>
      </td>
    );
  }
}

Cell.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.number,
  ]),
  header: PropTypes.string,
  data: PropTypes.object,
  row: PropTypes.object,
  onChange: PropTypes.func,
  componentProps: PropTypes.shape(),
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
  editable: PropTypes.bool,
  width: PropTypes.number,
  flexible: PropTypes.bool,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  tooltip: PropTypes.string,
};

Cell.defaultProps = {
  onChange: () => true,
  componentProps: {},
  component: null,
  editable: false,
  value: '',
  data: {},
  row: {},
  width: null,
  flexible: false,
  disabled: false,
  className: '',
  header: '',
  tooltip: '',
};

export default Cell;
