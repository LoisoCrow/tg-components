import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const AcceptancePeriod = ({ value }) => {
  value = +value;
  const className = classnames({
    'red': value === -1 || value === 0,
  });

  if (value === -1) {
    value = 'Истек';
  } else {
    value += 1;
  }

  return (
    <div className={className}>
      {value.toString()}
    </div>
  );
};

AcceptancePeriod.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number.isRequired,
    PropTypes.string.isRequired,
  ]).isRequired,
};

export default AcceptancePeriod;
