import React from 'react';
import { PropTypes } from 'prop-types';
import classnames from 'classnames';
import isFunction from 'lodash/isFunction';
import './Footer.sass';

const Footer = ({ children, right, className }) => {
  let rightComponent = null;
  if (right) {
    rightComponent = <div className="componentFooter__body-right">{isFunction(right) ? right() : right}</div>;
  }

  const componentClassName = classnames('componentFooter', { [className]: className });
  return (
    <div className={componentClassName}>
      <div className="componentFooter__body">
        {children}
        {rightComponent}
      </div>
    </div>
  );
};


Footer.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
  right: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
  className: PropTypes.string,
};

Footer.defaultProps = {
  children: null,
  right: null,
  className: null,
};

export default Footer;
