const kizCategories = [
  {
    kind_kiz: 1,
    sign_type: '2',
    origin_id: '1',
    sign_size_type: '1',
    label: 'Вшивной зеленый 53x101,6',
  },
  {
    kind_kiz: 2,
    sign_type: '3',
    origin_id: '1',
    sign_size_type: '1',
    label: 'Навесной зеленый 53x80',
  },
  {
    kind_kiz: 3,
    sign_type: '1',
    origin_id: '1',
    sign_size_type: '1',
    label: 'Клеевой зеленый 53x80',
  },
  {
    kind_kiz: 4,
    sign_type: '2',
    origin_id: '1',
    sign_size_type: '2',
    label: 'Вшивной зеленый длинный 25х170',
  },
  {
    kind_kiz: 5,
    sign_type: '3',
    origin_id: '1',
    sign_size_type: '2',
    label: 'Навесной зеленый длинный 25х160',
  },
  {
    kind_kiz: 6,
    sign_type: '1',
    origin_id: '1',
    sign_size_type: '2',
    label: 'Клеевой зеленый длинный 25х160',
  },
  {
    kind_kiz: 7,
    sign_type: '2',
    origin_id: '2',
    sign_size_type: '1',
    label: 'Вшивной красный 53x101,6',
  },
  {
    kind_kiz: 8,
    sign_type: '3',
    origin_id: '2',
    sign_size_type: '1',
    label: 'Навесной красный 53x80',
  },
  {
    kind_kiz: 9,
    sign_type: '1',
    origin_id: '2',
    sign_size_type: '1',
    label: 'Клеевой красный 53x80',
  },
  {
    kind_kiz: 10,
    sign_type: '2',
    origin_id: '2',
    sign_size_type: '2',
    label: 'Вшивной красный длинный 25х170',
  },
  {
    kind_kiz: 11,
    sign_type: '3',
    origin_id: '2',
    sign_size_type: '2',
    label: 'Вшивной красный длинный 25х170',
  },
  {
    kind_kiz: 12,
    sign_type: '1',
    origin_id: '2',
    sign_size_type: '2',
    label: 'Клеевой красный длинный 25х160',
  },
];

export const filterOptions = kizCategories.map(category => ({
  value: category.kind_kiz,
  name: category.label,
}));

export default kizCategories;
