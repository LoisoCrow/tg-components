import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import classnames from 'classnames';
import reduce from 'lodash/reduce';
import Checkbox from '../../../../components/Checkbox/Checkbox';
import './ToggleColumn.sass';

class ToggleColumn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      checkboxes: {},
    };
  }

  componentDidMount = () => {
    this.resetCheckboxes();
  };

  // componentWillReceiveProps = () => {
  //   this.resetCheckboxes();
  // };

  getCountChecked = () => (
    reduce(this.state.checkboxes, (result, value) => (result + value), 0)
  );

  getLabel = column => column.header;

  getClassLabel = isProtected => classnames('label', {
    label__disabled: isProtected,
  });

  resetCheckboxes = () => {
    const checkboxes = {};
    this.props.columns.forEach((currentValue) => {
      checkboxes[currentValue.field] = !currentValue.hidden;
    });
    this.setState({ checkboxes });
  };

  applyAction = () => {
    const { columns, toggleColumns } = this.props;
    const { checkboxes } = this.state;

    const newColumns = columns.map((column) => {
      if (checkboxes[column.field]) {
        return { ...column, hidden: false };
      }

      return { ...column, hidden: true };
    });

    toggleColumns(newColumns);
    this.handleClick();
  };

  handleClick = () => {
    if (!this.state.isFocused) {
      document.addEventListener('click', this.handleClickOutside, false);
    } else {
      document.removeEventListener('click', this.handleClickOutside, false);
    }

    this.setState(prevState => ({ isFocused: !prevState.isFocused }));
  };

  handleClickOutside = (e) => {
    const selectedBlock = this.parentComponent;

    if (e.path.includes(selectedBlock)) {
      return true;
    }

    this.handleClick();
    return true;
  };

  handleCheckboxChange = (checked, value) => {
    this.setState(prevState => ({
      checkboxes: {
        ...prevState.checkboxes,
        [value]: checked,
      },
    }));
  };

  renderOptions = () => {
    const { columns } = this.props;

    return columns.map((col) => {
      if (col.field && col.header) {
        return (
          <div className={this.getClassLabel(col.protected)} key={col.field}>
            <Checkbox
              value={col.field}
              name={col.field}
              label={this.getLabel(col)}
              checked={this.state.checkboxes[col.field]}
              disabled={col.protected}
              onChange={this.handleCheckboxChange}
            />
          </div>
        );
      }
      return null;
    });
  };

  render() {
    const presentationClasses = classnames('main_block', { __rotate: this.state.isFocused });
    const dropdownClasses = classnames('dropdown', { hide: !this.state.isFocused });
    return (
      <div
        className="component_toggle-column"
        ref={(node) => { this.parentComponent = node; }}
      >
        <div
          role="presentation"
          className={presentationClasses}
          onClick={this.handleClick}
        >
          {/* <span className="ico" /> */}
          <div className="toggleIcon" />
        </div>
        <div className={dropdownClasses}>
          <div className="tg_column __header">
            <span>Выбрано столбцов: <h4 className="count-checked">{this.getCountChecked()}</h4></span>
          </div>
          <div className="checks">{this.renderOptions()}</div>
          <div className="block block--buttons">
            <h4
              role="presentation"
              className="fn_btn fn_btn--apply link__action--blue"
              onClick={this.applyAction}
            >
              {'Применить'}
            </h4>
            <h4
              role="presentation"
              className="fn_btn fn_btn--reset link__action--grey"
              onClick={this.resetCheckboxes}
            >
              {'Сбросить'}
            </h4>
          </div>
        </div>
      </div>
    );
  }
}

ToggleColumn.propTypes = {
  toggleColumns: PropTypes.func.isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      header: PropTypes.string.isRequired,
      field: PropTypes.string.isRequired,
      hidden: PropTypes.bool,
    }),
  ).isRequired,
};

ToggleColumn.defaultProps = {
  isFocused: false,
  columns: {
    hidden: false,
  },
};

export default ToggleColumn;
