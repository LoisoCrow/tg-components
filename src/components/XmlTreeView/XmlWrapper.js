import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { cloneDeep } from 'lodash';
import parseXmlDOM from 'xml-parser';
import XmlTreeView from './XmlTreeView';
import XmlNumbersView from './XmlNumbersView';

class XmlWrapper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      xmlOptions: {},
    };
  }

  componentWillMount() {
    const { xmlDOM } = this.props;
    const parsedXml = this.parseXml(xmlDOM);
    this.setState({ xmlOptions: this.getXmlOptions(parsedXml, 1) });
  }

  onCollapse(valuesAsArray) {
    const newXmlOptions = cloneDeep(this.state.xmlOptions);
    let modifiedPiece = newXmlOptions;
    for (let i = 0; i < valuesAsArray.length - 1; i += 1) {
      const nextPiece = modifiedPiece.find(obj => obj.label === valuesAsArray[i]);
      if (nextPiece) {
        modifiedPiece = nextPiece.value;
      }
    }
    modifiedPiece = modifiedPiece.find(obj =>
      obj.label === valuesAsArray[valuesAsArray.length - 1]);
    modifiedPiece.collapsed = !modifiedPiece.collapsed;
    this.setState({ xmlOptions: newXmlOptions });
  }

  getXmlOptions = (xml, initialStartTagNumber) => {
    let nextStartTagNumber = initialStartTagNumber;
    return xml.map((tag) => {
      const startTagNumber = nextStartTagNumber;
      if (tag.children.length) {
        const currentXmlOptions = this.getXmlOptions(tag.children, startTagNumber + 1);
        const endTagNumber = currentXmlOptions[currentXmlOptions.length - 1].endTagNumber + 1;
        nextStartTagNumber = endTagNumber + 1;
        return {
          label: tag.name,
          attributes: tag.attributes,
          value: currentXmlOptions,
          collapsed: false,
          startTagNumber,
          endTagNumber,
        };
      }
      const endTagNumber = tag.content ? startTagNumber + 2 : startTagNumber + 1;
      nextStartTagNumber = endTagNumber + 1;
      return {
        label: tag.name,
        attributes: tag.attributes,
        value: tag.content,
        collapsed: false,
        startTagNumber,
        endTagNumber,
      };
    });
  }

  parseXml = xmlDOM => [parseXmlDOM(xmlDOM).root];

  renderNumbers = (xmlOptions, valuesAsArray) => (
    xmlOptions.map((option) => {
      const currentValuesAsArray = cloneDeep(valuesAsArray);
      currentValuesAsArray.push(option.label);
      return (<XmlNumbersView
        key={`${option.label}-${option.startTagNumber}`}
        value={typeof option.value === 'object'
          ? this.renderNumbers(option.value, currentValuesAsArray)
          : option.value}
        collapsed={option.collapsed}
        startTagNumber={option.startTagNumber}
        endTagNumber={option.endTagNumber}
        onChange={() => this.onCollapse(currentValuesAsArray)}
      />);
    }))

  renderXml = (xmlOptions, level, valuesAsArray) => (
    xmlOptions.map((option) => {
      const currentValuesAsArray = cloneDeep(valuesAsArray);
      currentValuesAsArray.push(option.label);
      return (<XmlTreeView
        key={`${option.label}-${option.startTagNumber}`}
        level={level}
        label={option.label}
        attributes={option.attributes}
        value={typeof option.value === 'object'
          ? this.renderXml(option.value, level + 1, currentValuesAsArray)
          : option.value}
        collapsed={option.collapsed}
        startTagNumber={option.startTagNumber}
        endTagNumber={option.endTagNumber}
        onChange={() => this.onCollapse(currentValuesAsArray)}
      />);
    })
  );

  render() {
    return (
      <div className="xmlNode__wrapper">
        <h1>{this.props.title}</h1>
        <div className="xmlNode__container">
          <div className="xmlNodeNumbers">{!!this.state.xmlOptions && this.renderNumbers(this.state.xmlOptions, [])}</div>
          <div className="xmlNodeList">{!!this.state.xmlOptions && this.renderXml(this.state.xmlOptions, 0, [])}</div>
        </div>
      </div>
    );
  }
}


XmlWrapper.propTypes = {
  xmlDOM: PropTypes.string.isRequired,
  title: PropTypes.string,
};

XmlWrapper.defaultProps = {
  xml: null,
  title: null,
};

export default XmlWrapper;
