import signState from './initialState';

export const reducerHandlers = types => ({
  [types.request]: state => ({
    ...state,
    loading: true,
    error: null,
    success: null,
    base64Xml: null,
    xml: null,
  }),
  [types.receive]: (state, action) => {
    const { xml, base64Xml } = action.payload;
    return {
      ...state,
      loading: false,
      success: true,
      xml,
      base64Xml,
    };
  },
  [types.error]: (state, action) => ({
    ...state,
    loading: false,
    success: false,
    error: action.payload,
    base64Xml: null,
    xml: null,
  }),
  [types.confirm]: state => ({
    ...state,
    signed: {
      loading: false,
      error: null,
      success: true,
    },
  }),
  [types.signRequest]: state => ({
    ...state,
    signed: {
      loading: true,
      error: null,
      success: null,
    },
  }),
  [types.signError]: (state, action) => ({
    ...state,
    signed: {
      loading: false,
      error: action.payload,
      success: false,
    },
  }),
});


export const createReducer = (actions) => {
  const create = (initialState, map) => (state = initialState, action) => {
    if (Object.hasOwnProperty.call(map, action.type)) {
      return map[action.type](state, action);
    }
    return state;
  };

  return create(signState, actions);
};
