import { formatNumber } from './format';

const moneyValueToRub = (value) => {
  value = +value;
  let cents = (value % 100).toString();
  const rubs = ((value - cents) / 100).toString();
  const parsedRubs = formatNumber(rubs);
  if (cents.length === 1) {
    cents += '0';
  }
  return `${parsedRubs}.${cents}`;
};

export default moneyValueToRub;
